from warnings import warn
import pygame

from EventCards import *

if __name__ == '__main__':
    from Project.EventCards import *

_debug = False


class Tile:

    def __init__(self, position: int, next_pos: int, rect: (int, int, int, int)):
        self.position = position
        self.next_pos = next_pos
        self.rect = pygame.Rect(rect)
        self.game = None

    def __next__(self):
        return self.next_position

    def __str__(self):
        return "%s\nPosition: %d\nNext: %d" % (str(type(self)), self.position, self.next_position)

    def landed_on_tile(self, player, *args, **kwargs):
        raise NotImplementedError()

    def moved_over_tile(self, player, *args, **kwargs):
        warn("This method should be overwritten, as is the method won't do anything")

    def get_player_position(self, player_index):
        """
        This assumes real_estate will never be on the corners
        :param player_index:
        :return:
        """
        i1_x, i1_y = 0.08 / 5 * 4, 0.14 * 2 / 3
        i2_x, i2_y = 0.08 / 5 * 3, 0.14 * 2 / 3
        i3_x, i3_y = 0.08 / 5 * 2, 0.14 * 2 / 3
        i4_x, i4_y = 0.08 / 5 * 1, 0.14 * 2 / 3
        i5_x, i5_y = 0.08 / 5 * 4, 0.14 * 1 / 3
        i6_x, i6_y = 0.08 / 5 * 3, 0.14 * 1 / 3
        i7_x, i7_y = 0.08 / 5 * 2, 0.14 * 1 / 3
        i8_x, i8_y = 0.08 / 5 * 1, 0.14 * 1 / 3

        if 1 < self.position < 11:
            base_x, base_y = 1 - 0.14 - (self.position - 2) * 0.08, 1 - 0.14 / 2
            if player_index == 1:
                return base_x + i1_x, base_y + i1_y
            if player_index == 2:
                return base_x + i2_x, base_y + i2_y
            if player_index == 3:
                return base_x + i3_x, base_y + i3_y
            if player_index == 4:
                return base_x + i4_x, base_y + i4_y
            if player_index == 5:
                return base_x + i5_x, base_y + i5_y
            if player_index == 6:
                return base_x + i6_x, base_y + i6_y
            if player_index == 7:
                return base_x + i7_x, base_y + i7_y
            if player_index == 8:
                return base_x + i8_x, base_y + i8_y

        elif 11 < self.position < 21:
            base_x, base_y = 0.14 / 2, (1 - 0.14) - (self.position - 12) * 0.08
            if player_index == 1:
                return base_x + i1_y, base_y + i1_x
            if player_index == 2:
                return base_x + i2_y, base_y + i2_x
            if player_index == 3:
                return base_x + i3_y, base_y + i3_x
            if player_index == 4:
                return base_x + i4_y, base_y + i4_x
            if player_index == 5:
                return base_x + i5_y, base_y + i5_x
            if player_index == 6:
                return base_x + i6_y, base_y + i6_x
            if player_index == 7:
                return base_x + i7_y, base_y + i7_x
            if player_index == 8:
                return base_x + i8_y, base_y + i8_x

        elif 21 < self.position < 31:
            base_x, base_y = 0.14 + (self.position - 22) * 0.08, 0.14 / 2
            if player_index == 1:
                return base_x + i1_x, base_y - i1_y
            if player_index == 2:
                return base_x + i2_x, base_y - i2_y
            if player_index == 3:
                return base_x + i3_x, base_y - i3_y
            if player_index == 4:
                return base_x + i4_x, base_y - i4_y
            if player_index == 5:
                return base_x + i5_x, base_y - i5_y
            if player_index == 6:
                return base_x + i6_x, base_y - i6_y
            if player_index == 7:
                return base_x + i7_x, base_y - i7_y
            if player_index == 8:
                return base_x + i8_x, base_y - i8_y

        elif 31 < self.position < 41:
            base_x, base_y = 1 - 0.14 / 2, 0.14 + (self.position - 32) * 0.08
            if player_index == 1:
                return base_x - i1_y, base_y - i1_x
            if player_index == 2:
                return base_x - i2_y, base_y - i2_x
            if player_index == 3:
                return base_x - i3_y, base_y - i3_x
            if player_index == 4:
                return base_x - i4_y, base_y - i4_x
            if player_index == 5:
                return base_x - i5_y, base_y - i5_x
            if player_index == 6:
                return base_x - i6_y, base_y - i6_x
            if player_index == 7:
                return base_x - i7_y, base_y - i7_x
            if player_index == 8:
                return base_x - i8_y, base_y - i8_x

        else:
            # TODO implement start, jail, jail_visit, free parking, go_to_jail
            #   implementation required in according class or needs to be manually set in GameFactory
            return 0.5, 0.5


class NoActionOnMoveOverTile(Tile):

    def moved_over_tile(self, player, *args, **kwargs):
        global _debug
        if _debug:
            print('Moved over', type(self), 'Tile on position', self.position)

    def landed_on_tile(self, player, *args, **kwargs):
        raise NotImplementedError()


class EventTile(NoActionOnMoveOverTile):

    def __init__(self, stack: EventCardStack, *args, **kwargs):
        Tile.__init__(self, *args, **kwargs)
        self.stack = stack

    def landed_on_tile(self, player, *args, **kwargs):
        # self.stack.draw(player, *args, **kwargs)
        pass


class CommunityTile(NoActionOnMoveOverTile):
    def __init__(self, stack: CommunityCardStack, *args, **kwargs):
        Tile.__init__(self, *args, **kwargs)
        self.stack = stack

    def landed_on_tile(self, player, *args, **kwargs):
        # self.stack.draw(player, *args, **kwargs)
        pass


class StartTile(Tile):

    def landed_on_tile(self, player, *args, **kwargs):
        pass

    def moved_over_tile(self, player, *args, **kwargs):
        player.cash += 200


class JailTile(NoActionOnMoveOverTile):

    def landed_on_tile(self, player, *args, **kwargs):
        pass


class JailVisitTile(NoActionOnMoveOverTile):

    def landed_on_tile(self, player, *args, **kwargs):
        pass


class FreeTile(NoActionOnMoveOverTile):

    def landed_on_tile(self, player, *args, **kwargs):
        pass


class TaxTile(NoActionOnMoveOverTile):

    def __init__(self, amount, *args, **kwargs):
        Tile.__init__(self, *args, **kwargs)
        self.amount = amount

    def landed_on_tile(self, player, *args, **kwargs):
        player.cash -= self.amount


class ToJailTile(NoActionOnMoveOverTile):

    def landed_on_tile(self, player, *args, **kwargs):
        player.target_position = 41


class RealEstate(NoActionOnMoveOverTile):

    RAILWAY = 'railway'
    UTILITY = 'utility'

    class Building:

        def __init__(self, cost):
            self.is_build = False
            self.cost = cost

    def __init__(self, name, value, mortgage, rent, group, houses=(), owner=None, *args, **kwargs):
        Tile.__init__(self, *args, **kwargs)
        self.group = group
        self.name = name
        self.value = value
        self.mortgage = mortgage
        self.is_mortgaged = False
        self.rent = rent
        self.houses = houses
        self.owner = owner

    def __str__(self):
        out = super().__str__()
        out += "\n%s" % self.name
        out += "Rent: %d" % self.get_rent(1, 1)
        return out

    def get_build(self):
        if self.houses is None:
            return 0
        else:
            build = 0
            for h in self.houses:
                if h.is_build:
                    build += 1
            return build

    def get_rent(self, *args, **kwargs):
        if hasattr(self.rent, '__call__'):
            return self.rent(self, *args, **kwargs)
        else:
            count = 0
            for el in self.game.get_real_estate_by_owner(self.owner):
                if el.group == self.group:
                    count += 1
            if count == self.game.get_real_estate_group_count(self.group):
                if not self.houses[0].is_build:
                    return self.rent[0] * 2
                else:
                    if self.houses[4].is_build:
                        return self.rent[5]
                    else:
                        for i in range(len(self.houses)):
                            if not self.houses[i].is_build:
                                return self.rent[i]
                            return self.rent[-2]
            else:
                return self.rent[0]

    def landed_on_tile(self, player, *args, **kwargs):
        if _debug:
            print("Landed on Tile", type(self), "Tile on position", self.position)

        if self.owner is None:
            player.buy_real_estate(self)

        elif self.owner != player:
            if not self.is_mortgaged:
                rent = self.get_rent(d1=player.dice[0], d2=player.dice[1], *args, **kwargs)
                player.cash -= rent
                self.owner.cash += rent

    def railway_rent(self, *args, **kwargss):
        count = 0
        for s in self.game.get_real_estate_by_owner(self.owner):
            if s.group == RealEstate.RAILWAY:
                count += 1
        rent = 25
        for i in range(count - 1):
            rent *= 2
        return rent

    def utility_rent(self, d1, d2, *args, **kwargs):
        count = 0
        for el in self.game.get_real_estate_by_owner(self.owner):
            if el.group == RealEstate.UTILITY:
                count += 1
        return (d1 + d2) * 4 if count == 1 else 10
