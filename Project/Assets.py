import os

import pygame

board = None
figures = None


def load():
    path = os.path.dirname(__file__)
    assets = os.path.join(path, 'Assets')

    global board
    board = pygame.image.load(os.path.join(assets, 'Board.png')).convert()

    global figures
    figures = (pygame.image.load(os.path.join(assets, 'dogo.png')).convert_alpha(),
               pygame.image.load(os.path.join(assets, 'eisen.png')).convert_alpha(),
               pygame.image.load(os.path.join(assets, 'fingerH.png')).convert_alpha(),
               pygame.image.load(os.path.join(assets, 'hat.png')).convert_alpha(),
               pygame.image.load(os.path.join(assets, 'karre.png')).convert_alpha(),
               pygame.image.load(os.path.join(assets, 'lambo.png')).convert_alpha(),
               pygame.image.load(os.path.join(assets, 'ship.png')).convert_alpha(),
               pygame.image.load(os.path.join(assets, 'shoe.png')).convert_alpha()
               )
