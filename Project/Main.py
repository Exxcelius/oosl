import os
import time

import pygame
from pygame.locals import *

import Assets
from Game import *
from Menus import *
from Player import *

if __name__ != '__main__':
    from Project.Player import *
    from Project.Menus import *
    from Project.Game import *

# path variables
path = os.path.dirname(__file__)
assets = os.path.join(path, 'Assets')

#
pygame.init()
win_x, win_y = 1600, 1000
win = pygame.display.set_mode((win_x, win_y))

logo = pygame.image.load(os.path.join(assets, 'logo.jpg')).convert()
logo = pygame.transform.scale(logo, (1600,1000))
win.blit(logo, logo.get_rect())
pygame.display.update()

# This is the place to load all necessary Assets
Assets.load()
menu = pygame.Surface((win_x, win_y))
menu.fill((255, 255, 255))

min_logo_time = 2
now = time.time()
while 1:
    if time.time() - now > min_logo_time:
        break

win.blit(menu, menu.get_rect())
pygame.display.update()

# Preparation for efficient run of gameloop
# TODO: Discuss use of view Methods
run = True
rects_to_update = []

while run:
    result = main_menu()
    print(result)

    if result is None:
        run = False
    else:
        game = ClassicGermanGameFactory().create_game(result)
        game.play()

    for e in pygame.event.get():
        if e.type == QUIT:
            run = False
        elif e.type == MOUSEBUTTONDOWN:
            pass
        elif e.type == KEYDOWN:
            if e.key == K_ESCAPE:
                pygame.event.post(pygame.event.Event(QUIT))
pygame.quit()
exit(0)