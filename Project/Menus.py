import pygame
from pygame.locals import *

import sys

from Player import *
from Game import *

if __name__ == '__main__':
    from Project.Player import *
    from Project.Game import *

from Game import *

if __name__ == "__main__":
    from Project.Game import *

# initializing
mainClock = pygame.time.Clock()

pygame.init()
pygame.display.set_caption('game base')
win = pygame.display.set_mode((1600, 1000), 0, 32)

font = pygame.font.SysFont("Consolas", 100)


# write Text on coordinates or x-centred when x=='c'
def draw_text(text, font, color, surface, x, y):
    textobj = font.render(text, 1, color)
    textrect = textobj.get_rect()
    if x == 'c':
        textrect.topleft = (800 - textobj.get_rect().width / 2, y)
    else:
        textrect.topleft = (x, y)
    surface.blit(textobj, textrect)


# Main Menu, is opened if game is started
def main_menu():
    click = False

    font = pygame.font.SysFont("Consolas", 88)

    button_play = pygame.Rect(600, 300, 400, 120)
    button_rules = pygame.Rect(600, 500, 400, 120)
    button_settings = pygame.Rect(600, 700, 400, 120)
    button_quit = pygame.Rect(700, 900, 200, 100)

    win.fill((0, 0, 0))
    draw_text('Monopoly', pygame.font.SysFont("Consolas", 200), (255, 255, 255), win, 'c', 60)
    while True:
        mx, my = pygame.mouse.get_pos()

        # PLAY button
        # if mouse hovers over button
        if button_play.collidepoint((mx, my)):
            # if mouse hovers over the button its color is changed
            pygame.draw.rect(win, (200, 15, 15), button_play)
            # if mouse clicks
            if click:
                players = play_menu()
                # if player_list is return
                if players != 0:
                    return players
                # if game is quit
                if players is None:
                    return None
                # resetting the screen when go back to the main menu from another menu
                win.fill((0, 0, 0))
                draw_text('Monopoly', pygame.font.SysFont("Consolas", 200), (255, 255, 255), win, 'c', 60)
        # standart color of button if the mosue does not hover over it
        else:
            pygame.draw.rect(win, (255, 0, 0), button_play)
        draw_text('Play', font, (255, 255, 255), win, 'c', 320)

        # RULES button
        if button_rules.collidepoint((mx, my)):
            pygame.draw.rect(win, (200, 15, 15), button_rules)
            if click:
                rules = rules_menu()
                # if game is quit
                if rules is None:
                    return None
                win.fill((0, 0, 0))
                draw_text('Monopoly', pygame.font.SysFont("Consolas", 200), (255, 255, 255), win, 'c', 60)

        else:
            pygame.draw.rect(win, (255, 0, 0), button_rules)
        draw_text('Rules', font, (255, 255, 255), win, 'c', 520)

        if button_settings.collidepoint((mx, my)):
            pygame.draw.rect(win, (200, 0, 0), button_settings)
            if click:
                options = options_menu()
                # if game is quit
                if options is None:
                    return None
                win.fill((0, 0, 0))
                draw_text('Monopoly', pygame.font.SysFont("Consolas", 200), (255, 255, 255), win, 'c', 60)

        else:
            pygame.draw.rect(win, (255, 0, 0), button_settings)
        draw_text('Options', font, (255, 255, 255), win, 'c', 720)

        # QUIT button
        if button_quit.collidepoint((mx, my)):
            pygame.draw.rect(win, (50, 50, 50), button_quit)
            # if game is quit
            if click:
                return None
        else:
            pygame.draw.rect(win, (0, 0, 0), button_quit)
        draw_text('Quit', font, (255, 255, 255), win, 'c', 910)

        click = False

        # Event handling
        for event in pygame.event.get():
            if event.type == QUIT:
                return None
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return None
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        pygame.display.update()
        mainClock.tick(60)


def play_menu():
    click = False
    win.fill((0, 0, 0))

    font = pygame.font.SysFont("Consolas", 55)
    draw_text('Chose your Players', pygame.font.SysFont("Consolas", 100), (255, 255, 255), win, 'c', 50)

    button_p1 = pygame.Rect(100, 300, 275, 130)
    button_p2 = pygame.Rect(475, 300, 275, 130)
    button_p3 = pygame.Rect(850, 300, 275, 130)
    button_p4 = pygame.Rect(1225, 300, 275, 130)

    button_p5 = pygame.Rect(100, 500, 275, 130)
    button_p6 = pygame.Rect(475, 500, 275, 130)
    button_p7 = pygame.Rect(850, 500, 275, 130)
    button_p8 = pygame.Rect(1225, 500, 275, 130)

    button_quit = pygame.Rect(1300, 900, 200, 100)
    button_start_game = pygame.Rect(100, 750, 650, 150)

    # List for all Player buttons, map with player types "mapped" on buttons list
    buttons_p = [button_p1, button_p2, button_p3, button_p4, button_p5, button_p6, button_p7, button_p8]
    player_list = [2]+[1] * 7
    # player Types in Text form
    figures = ["None", "Player", "AiPlayer"]
    color_array = [(94, 94, 94), (115, 115, 115), (24, 116, 205), (0, 154, 205), (34, 139, 34), (50, 205, 50)]

    while True:
        mx, my = pygame.mouse.get_pos()

        # placing, color selecting, text selecting for 8 player buttons
        for i in range(len(player_list)):
            if buttons_p[i].collidepoint((mx, my)):
                pygame.draw.rect(win, color_array[(player_list[i] * 2) - 1], buttons_p[i])
                if click:
                    player_list[i] = (player_list[i] + 1) % len(figures)
            else:
                pygame.draw.rect(win, color_array[(player_list[i] * 2) - 2], buttons_p[i])
            textobj = font.render(figures[player_list[i] - 1], 1, (255, 255, 255))
            textrect = textobj.get_rect()
            textrect.center = buttons_p[i].center
            win.blit(textobj, textrect)

        # START GAME button
        if button_start_game.collidepoint((mx, my)):
            player_count = 0
            for player in player_list:
                if player in (2, 0):
                    player_count += 1
            if player_count >= 2:
                pygame.draw.rect(win, (200, 15, 15), button_start_game)
                if click:
                    out = [None if i == 1 else Player if i == 2 else AiPlayer for i in player_list]
                    return out
            else:
                pygame.draw.rect(win, (50, 50, 50), button_start_game)
        else:
            pygame.draw.rect(win, (255, 0, 0), button_start_game)
        textobj = pygame.font.SysFont("Consolas", 85).render('Start Game!', 1, (255, 255, 255))
        textrect = textobj.get_rect()
        textrect.center = button_start_game.center
        win.blit(textobj, textrect)

        # QUIT button, return to main menu
        if button_quit.collidepoint((mx, my)):
            pygame.draw.rect(win, (50, 50, 50), button_quit)
            if click:
                return 0
        else:
            pygame.draw.rect(win, (0, 0, 0), button_quit)
        textobj = font.render('Back', 1, (255, 255, 255))
        textrect = textobj.get_rect()
        textrect.center = button_quit.center
        win.blit(textobj, textrect)

        click = False
        # Event handling
        for event in pygame.event.get():
            if event.type == QUIT:
                return None
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return 0
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        pygame.display.update()
        mainClock.tick(60)


def rules_menu():
    click = False
    win.fill((0, 0, 0))

    font = pygame.font.SysFont("Consolas", 30)
    draw_text('Rules', pygame.font.SysFont("Consolas", 100), (255, 255, 255), win, 'c', 50)

    button_quit = pygame.Rect(1300, 900, 200, 100)

    rules = [
        'Jeder Spieler erhält zu Spielbeginn eine Figur und1.500 Euro Startkapital.',
        'Die Ereignis- und die Gemeinschaftskarten werden verdeckt auf dem Brett platziert.',
        'Alle Mitspieler starten auf dem Feld "Los". Gezogen wird im Uhrzeigersinn.',
        'Es wird mit zwei Würfeln gewürfelt. Es wird um die Gesamtsumme der Augen gezogen.',
        'Würfelt er einen Pasch, darf er anschließend noch einmal würfeln und ziehen.',
        'Würfelt ein Spieler dreimal hintereinander einen Pasch,',
        '    muss er sich auf das Feld "Gefängnis" begeben.',
        'Landet ein Spieler auf einer Straße die niemanden gehört, kann dieser sie kaufen.',
        'Landen danach andere Spieler auf der Straße, müssen sie dem Besitzer Miete zahlen.',
        'Möchte oder kann er die Straße nicht kaufen,wird die Straße',
        '    unter den anderen Spieler versteigert.',
        'Besitzt ein Spieler alle Straßen einer Farbe, kann er darauf Häuser bauen. ',
        'Für jedes Haus erhöht sich die Miete. Bei vier Häusern auf einer Straße,',
        '    kann ein Hotel gebaut werden.',
        'Landet ein Spieler auf einem Ereignis- oder Gemeinschaftsfeld,',
        '    muss er die oberste Karte gezogen werden.',
        'Landet ein Spieler durch Würfeln auf dem Gefängnisfeld, passiert nichts.',
        'Um aus dem Gefängnis frei zu kommen, muss der Spieler einen Pasch würfeln.',
        'Er kann außerdem die Karte "Du kommst aus dem Gefängnis frei" nutzen.',
        'Passiert ein Spieler das Feld "Los", erhält er jedes mal 200 Euro.',
        'Ein Spieler scheidet aus dem Spiel aus, sobald er bankrott geht.',
        'Der letze nicht ausgeschiedene Spieler gewint das Spiel.',
        '',
        'Viel Spaß!'
    ]

    while True:
        mx, my = pygame.mouse.get_pos()

        # placing the rules on the screen
        for i, rule in enumerate(rules):
            textobj = font.render(rule, 1, (255, 255, 255))
            textrect = textobj.get_rect()
            textrect.topleft = (100, 200 + 30 * i)
            win.blit(textobj, textrect)

        # QUIT button, return to main menu
        if button_quit.collidepoint((mx, my)):
            pygame.draw.rect(win, (50, 50, 50), button_quit)
            if click:
                return True
        else:
            pygame.draw.rect(win, (0, 0, 0), button_quit)
        textobj = pygame.font.SysFont("Consolas", 55).render('Back', 1, (255, 255, 255))
        textrect = textobj.get_rect()
        textrect.center = button_quit.center
        win.blit(textobj, textrect)

        click = False
        # Event handling
        for event in pygame.event.get():
            if event.type == QUIT:
                return None
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return True
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        pygame.display.update()
        mainClock.tick(60)

"""
def options_menu():
    return False
"""