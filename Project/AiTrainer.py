import neat
import os
import pygame
#import pickle

from Player import *
from Game import ClassicGermanGameFactory
import Assets


if __name__ != "__main__":
    from Project.Game import *
    from Project.Player import *


def eval_genomes(genomes, config):
    # Setup for pygame artifacts to work
    # TODO: split pygame and player logic
    win = pygame.display.set_mode((1, 1))


    Assets.load()
    players = []
    running_genomes = []
    for x, ge in genomes:
        ge.fitness = 0
        players.append(AiPlayer(ge, config))
        running_genomes.append(ge)

    game = ClassicGermanGameFactory.create_game(players)

    count = 0
    while True:
        for player in game.players:
            #print("Player", player.index)
            while True:
                try:
                    player.move(dice())

                    # update player position
                    while True:
                        if not player.target_position == 41:
                            cur_tile = player.game.get_tile_by_position(player.position)
                            next_tile = player.game.get_tile_by_position(next(cur_tile))
                            player.position = next_tile.position
                            next_tile.moved_over_tile(player)
                        else:
                            player.position = 41
                        if player.position == player.target_position:
                            next_tile.landed_on_tile(player)
                            break

                    for re in player.real_estate:
                        re = re[0] if type(re) is list else re
                        player.build_houses(re)
                        player.sell_houses(re)
                        player.mortgage_real_estate(re)
                        player.de_mortgage_real_estate(re)
                    if player.doublets == 0:
                        break
                except NotEnoughMoneyError:
                    for re in player.real_estate:
                        player.sell_houses(re)
                        player.mortgage_real_estate(re)
                    if player.cash < 0:
                        game.players.remove(player)
                        print(player, count)
                        break
            mult = -0.01 * count + 1
            player.genome.fitness += player.net_worth() * (1 + mult if mult > 0 else 0)

        count += 1

        if len(game.players) <= 1:
            if len(game.players) == 1:
                game.players[0].genome.fitness *= 8
            break
        if count > 1000:
            break

    for p in players:
        print(p, p.genome.fitness)

def run(config_file, pickle_path):
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)

    p = neat.Population(config)
    p.add_reporter(neat.StdOutReporter(True))
    p.add_reporter(neat.StatisticsReporter())

    winner = None

    try:
        winner = p.run(eval_genomes, 100)
        print(type(winner))
    except RuntimeError as e:
        print(e)

    # winner network gets to be pickled
    with open(pickle_path, "wb") as file:
        #network =
        pickle.dump(winner, file)


if __name__ == "__main__":
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, "config-forward.txt")
    pickle_file = os.path.join(local_dir, "ANN_Genome.pickle")
    run(config_path, pickle_file)
