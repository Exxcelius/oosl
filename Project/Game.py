import sys
from itertools import cycle
import pygame
from pygame.locals import *

import Assets
from EventCards import ClassicGermanEventCardStackFactory, ClassicGermanCommunityCardStackFactory
from Player import *
from Tiles import *
from Utility import *

if __name__ == "__main__":
    from Project.Tiles import *
    from Project.Player import *
    from Project.EventCards import ClassicGermanEventCardStackFactory, ClassicGermanCommunityCardStackFactory
    from Project.Utility import *

class Game:

    pygame.init()

    def __init__(self, tiles, starting_cash, players, win=(1600, 1000)):
        self.players = []
        for x, player in enumerate(players):
            if type(player) is type:
                if player is Player:
                    self.players.append(Player(cash=starting_cash, index=x + 1, game=self))
                elif player is AiPlayer:
                    self.players.append(cash=starting_cash, index=x + 1, game=self)
            else:
                self.players.append(player)
                player.index = x + 1
                player.cash = starting_cash
                player.game = self
        self.tiles = tiles
        for tile in self.tiles:
            tile.game = self

        self.win_x = win[0]
        self.win_y = win[1]
        self.win = pygame.display.set_mode(win)

    def get_real_estate_by_owner(self, owner):
        for tile in self.tiles:
            if isinstance(tile, RealEstate):
                if tile.owner == owner:
                    yield tile

    def get_tile_by_position(self, position):
        for tile in self.tiles:
            if tile.position == position:
                return tile

    def get_real_estate_group_count(self, group):
        count = 0
        for tile in self.tiles:
            if tile is RealEstate:
                if tile.group == group:
                    count += 1
        return count

    def play(self):
        players = [player for player in self.players if player is not None]

        player_group = pygame.sprite.Group()
        player_group.add(players)

        player_pool = MyCycle(players)
        player = next(player_pool)

        # draw information about all players on the left panel site
        buttons_p = [pygame.Rect(1000, 125 * i, 600, 125) for i in range(len(players))]

        font = pygame.font.SysFont("Consolas", 30)

        def draw_sidebar():
            for i, player_iterate in enumerate(self.players):
                # Fields for each player
                pygame.draw.rect(win, (213, 233, 206), buttons_p[i])
                # boarder
                pygame.draw.rect(win, (0, 0, 0), buttons_p[i], 4)
                # player icon centred y and manuel for x (with max 125)
                if player_iterate.index == player.index:
                    win.blit(pygame.transform.scale(player_iterate.image, (100,100)), (1012, (buttons_p[i].centery)-25))
                else:
                    win.blit(pygame.transform.scale(player_iterate.image, (60,60)), (1032, (buttons_p[i].centery-15)))
                # Player Name TODO: positioning and writing "houses" , loading of correct Player images
                if player_iterate.index == player.index:
                    textobj = pygame.font.SysFont("Consolas", 25).render("Player " + str(i+1), 1, (20, 20, 230))
                else:
                    textobj = pygame.font.SysFont("Consolas", 25).render("Player " + str(i+1), 1, (0, 0, 0))
                textrect = textobj.get_rect()
                textrect.topleft = (buttons_p[i].left + 34, buttons_p[i].centery - 54)
                win.blit(textobj, textrect)

                textobj = pygame.font.SysFont("Consolas", 25).render("Money: " + str(self.players[i].cash) + " $", 1, (0, 0, 0))
                textrect = textobj.get_rect()
                textrect.topleft = (buttons_p[i].left + 134, buttons_p[i].centery - 24)
                win.blit(textobj, textrect)


                #JUST A TEST SO DELET IF NOT NEEDED ANYMORE
                # for t in self.tiles:
                #    pygame.draw.rect(win, (255, 0, 0), t, 4)

        def clicked_tile(t):
            #TODO: Tile Handling check type of Tilte to functions
            print(str(t.position))


        print("I'm doing it!")

        pygame.init()
        pygame.display.set_caption('Monopoly')
        win = pygame.display.set_mode((1600, 1000), 0, 32)
        win.blit(Assets.board, Assets.board.get_rect())
        clock = pygame.time.Clock()

        dialogs = FrameList()
        no_updates = False
        created_dialogs = False

        while True:
            click = False
            mx, my = pygame.mouse.get_pos()

            for event in pygame.event.get():
                if event.type == QUIT:
                    return None
                if event.type == MOUSEBUTTONDOWN:
                    if event.button == 1:
                        click = True

            if player.finished_turn:
                player.dice = (0, 0)
                player = next(player_pool)
                player.finished_turn = False
                created_dialogs = False
                dialogs.clear()
                continue

            if player.dice != (0, 0):
                new_dice = dice()
                player.move(new_dice)

            try:
                if player.position == player.target_position:
                    tile = self.get_tile_by_position(player.position)

                    if type(tile) is RealEstate:
                        if tile.owner is None:
                            if not created_dialogs:
                                dialogs.add(YNDialog("Buying", "Bla", color = (0, 0, 0)))
                                created_dialogs = True
                            else:
                                # TODO: implement some way to memorize a dialog instance and retrieve it
                                if not dialogs.items[0].visible:
                                    if dialogs.items[0].click_result:
                                        player.buy_real_estate(tile)

                        elif tile.owner != player:
                            dialogs.add(Dialog("Miete", "Du musstest %d$ Miete\n an Spieler %d bezahlen" %
                                               (tile.get_rent(), tile.owner.index),
                                               layer=1, rect=pygame.Rect(200, 200, 300, 300)))
                            created_dialogs = True

                    elif type(tile) is TaxTile:
                        dialogs.add(Dialog("Steuern", "Du musst %d$\nan die Bank bezahlen", layer=1,
                                           rect=pygame.rect.Rect(200, 200, 300, 300)))
                        created_dialogs = True

                    elif type(tile) is ToJailTile:
                        if no_updates:
                            if not dialogs.items[0].visible:
                                no_updates = False
                        elif len(dialogs.items) == 0:
                            dialogs.add(Dialog("Gefängnis", "Gehe direkt in das Gefängnis", layer=1,
                                               rect=pygame.rect.Rect(200, 200, 300, 300)))
                            no_updates = True

                    elif type(tile) is EventTile:
                        card = tile.stack.draw()
                        if card.is_option_dialog:
                            pass
                        else:


                    elif type(tile) is CommunityTile:
                        pass

            except NotEnoughMoneyWarning:
                pass
            except NotEnoughMoneyError:
                pass

            """
        
        else:

            # TODO: look if this breaks things



            while 1:
                click = False
                for event in pygame.event.get():
                    if event.type == QUIT:
                        pygame.quit()
                        sys.exit()
                    if event.type == KEYDOWN:
                        if event.key == K_ESCAPE:
                            pygame.quit()
                            sys.exit()
                    if event.type == MOUSEBUTTONDOWN:
                        if event.button == 1:
                            click = True
                if click:
                    if player.finished_turn:
                        player.finished_turn = False
                        player = next(player_pool)
                        print("clicked", player.index)
                        try:
                            player.move(dice())

                        except NotEnoughMoneyWarning:
                            pass  # Implemented GUI Warning
                        except NotEnoughMoneyError:
                            self.players.remove(player)
                            player = next(player_pool)
                            player_pool = cycle(self.players)
                            while True:
                                temp = next(player_pool)
                                if temp == player:
                                    break
                            if len(self.players) <= 1:
                                pass  # Remaining player wins"""
            if not no_updates:
                for p in self.players:
                    p.update()

            # Michis Zeile
            win.fill((0, 0, 0))
            win.blit(Assets.board, Assets.board.get_rect())
            draw_sidebar()
            # MICHIS TILE TEST
            for t in self.tiles:
                if t.rect.collidepoint((mx, my)):
                    pygame.draw.rect(win, (255, 0, 0), t.rect, 5)
                    if click:
                        clicked_tile(t)

            tmp = Dialog(title="Miete", text="Du musstest %d$ Miete\n an %s zahlen!", layer=1,
                         rect=pygame.rect.Rect(200, 200, 600, 300))
            tmp.draw(win)
            # MICHIS TILE TEST END
            player_group.draw(win)
            pygame.display.update()
            clock.tick(5)

    def get_player_position(self, player, pos_index, offset=(0, 0)):
        for t in self.tiles:
            if t.position == pos_index:
                board_width, board_height = Assets.board.get_size()
                pos_x, pos_y = t.get_player_position(player.index)
                return int(pos_x * board_width + offset[0]), int(pos_y * board_height + offset[1])


class GameFactory:

    @staticmethod
    def create_game(players):
        """
        This creates a game with the given players
        Needs to be implemented by Subclasses
        :param players: the players for the Game 
        :return: an instance of class Game
        """
        raise NotImplementedError()


class ClassicGermanGameFactory(GameFactory):

    @staticmethod
    def create_game(players):
        house50 = (RealEstate.Building(50) for i in range(5))
        house100 = (RealEstate.Building(100) for i in range(5))
        house150 = (RealEstate.Building(150)for i in range(5))
        house200 = (RealEstate.Building(200) for i in range(5))

        event_stack = ClassicGermanEventCardStackFactory().create_stacks()
        community_stack = ClassicGermanCommunityCardStackFactory().create_stacks()

        # calculating positions of all tiles
        # s for size of the gameboard  -  sxs
        s = 1000
        rects = []

        rects.append((int(s/12*10.5), int(s/12*10.5), int(s/12*1.5 +1), int(s/12*1.5 +1)))
        for i in range(9):
            rects.append((int(s / 12 * (9.5-i)), int(s / 12 * 10.5), int(s / 12 * 1 + 1), int(s / 12 * 1.5 + 1)))

        rects.append((0, int(s/12*10.5), int(s/12*1.5 +1), int(s/12*1.5 +1)))
        for i in range(9):
            rects.append((0, int(s / 12 * (9.5-i)), int(s / 12 * 1.5 + 1), int(s / 12 * 1 + 1)))

        rects.append((0, 0, int(s/12*1.5 +1), int(s/12*1.5 +1)))
        for i in range(9):
            rects.append((int(s / 12 * (1.5+i)), 0, int(s / 12 * 1 + 1), int(s / 12 * 1.5 + 1)))

        rects.append((int(s/12*10.5), 0, int(s/12*1.5 +1), int(s/12*1.5 +1)))
        for i in range(9):
            rects.append((int(s / 12 * 10.5), int(s / 12 * (1.5+i)), int(s / 12 * 1.5 + 1), int(s / 12 * 1 + 1)))

        tiles = (StartTile(position=1, next_pos=2, rect=rects[0]),
                 RealEstate(name='Badstrasse', value=60, mortgage=30, rent=(2, 10, 30, 90, 160, 250), group=1, position=2, next_pos=3, rect=rects[1], houses=house50),
                 CommunityTile(position=3, next_pos=4, rect=rects[2], stack=community_stack),
                 RealEstate(name='Turmstrasse', value=60, mortgage=30, rent=(4, 20, 60, 180, 320, 450), group=1, position=4, next_pos=5, rect=rects[3], houses=house50),
                 TaxTile(amount=200, position=5, next_pos=6, rect=rects[4]),
                 RealEstate(name='Südbahnhof', value=200, mortgage=30, rent=RealEstate.railway_rent, group=RealEstate.RAILWAY, position=6, next_pos=7, rect=rects[5], houses=None),
                 RealEstate(name='Chausseestrasse', value=100, mortgage=50, rent=(6, 30, 90, 270, 400, 550), group=3, position=7, next_pos=8, rect=rects[6], houses=house50),
                 EventTile(position=8, next_pos=9, rect=rects[7], stack=event_stack),
                 RealEstate(name='Elisenstrasse', value=100, mortgage=50, rent=(6, 30, 90, 270, 400, 550), group=3, position=9, next_pos=10, rect=rects[8], houses=house50),
                 RealEstate(name='Poststrasse', value=120, mortgage=60, rent=(8, 40, 100, 300, 450, 600), group=3, position=10, next_pos=11, rect=rects[9], houses=house50),
                 JailVisitTile(position=11, next_pos=12, rect=rects[10]),
                 RealEstate(name='Seestrasse', value=140, mortgage=70, rent=(10, 50, 150, 450, 625, 750), group=4, position=12, next_pos=13, rect=rects[11], houses=house100),
                 RealEstate(name='Elektrizitätswerk', value=150, mortgage=75, rent=RealEstate.utility_rent, group=RealEstate.UTILITY, position=13, next_pos=14, rect=rects[12], houses=None),
                 RealEstate(name='Hafenstrasse', value=140, mortgage=70, rent=(10, 50, 150, 450, 625, 750), group=4, position=14, next_pos=15, rect=rects[13], houses=house100),
                 RealEstate(name='Neuestrasse', value=160, mortgage=80, rent=(12, 60, 180, 500, 700, 900), group=4, position=15, next_pos=16, rect=rects[14], houses=house100),
                 RealEstate(name='Westbahnhof', value=200, mortgage=100, rent=RealEstate.railway_rent, group=RealEstate.RAILWAY, position=16, next_pos=17, rect=rects[15], houses=None),
                 RealEstate(name='Münchener Strasse', value=180, mortgage=90, rent=(14, 70, 200, 550, 750, 950), group=5, position=17, next_pos=18, rect=rects[16], houses=house100),
                 CommunityTile(position=18, next_pos=19, rect=rects[17], stack=community_stack),
                 RealEstate(name='Wiener Strasse', value=180, mortgage=90, rent=(14, 70, 200, 550, 750, 950), group=5, position=19, next_pos=20, rect=rects[18], houses=house100),
                 RealEstate(name='Berliner Strasse', value=200, mortgage=100, rent=(16, 80, 220, 600, 800, 1000), group=5, position=20, next_pos=21, rect=rects[19], houses=house100),
                 FreeTile(position=21, next_pos=22, rect=rects[20]),
                 RealEstate(name='Theaterstrasse', value=220, mortgage=110, rent=(18, 90, 250, 700, 875, 1050), group=6, position=22, next_pos=23, rect=rects[21], houses=house150),
                 EventTile(position=23, next_pos=24, rect=rects[22], stack=event_stack),
                 RealEstate(name='Museumstrasse', value=220, mortgage=110, rent=(18, 90, 250, 700, 875, 1050), group=6, position=24, next_pos=25, rect=rects[23], houses=house150),
                 RealEstate(name='Opernplatz', value=240, mortgage=120, rent=(20, 100, 300, 750, 925, 1100), group=6, position=25, next_pos=26, rect=rects[24], houses=house150),
                 RealEstate(name='Nordbahnhof', value=200, mortgage=100, rent=RealEstate.railway_rent, group=RealEstate.RAILWAY, position=26, next_pos=27, rect=rects[25], houses=None),
                 RealEstate(name='Lessingstrasse', value=260, mortgage=130, rent=(22, 110, 330, 800, 975, 1150), group=7, position=27, next_pos=28, rect=rects[26], houses=house150),
                 RealEstate(name='Schillerstrasse', value=260, mortgage=130, rent=(22, 110, 330, 800, 975, 1150), group=7, position=28, next_pos=29, rect=rects[27], houses=house150),
                 RealEstate(name='Wasserwerk', value=150, mortgage=75, rent=RealEstate.utility_rent, group=RealEstate.UTILITY, position=29, next_pos=30, rect=rects[28], houses=house150),
                 RealEstate(name='Goethestrasse', value=280, mortgage=140, rent=(24, 120, 360, 850, 1025, 1200), group=7, position=30, next_pos=31, rect=rects[29], houses=house150),
                 ToJailTile(position=31, next_pos=32, rect=rects[30]),
                 RealEstate(name='Rathausplatz', value=300, mortgage=150, rent=(26, 130, 390, 900, 1100, 1275), group=8, position=32, next_pos=33, rect=rects[31], houses=house200),
                 RealEstate(name='Hauptstrasse', value=300, mortgage=150, rent=(26, 130, 390, 900, 1100, 1275), group=8, position=33, next_pos=34, rect=rects[32], houses=house200),
                 CommunityTile(position=34, next_pos=35, rect=rects[33], stack=community_stack),
                 RealEstate(name='Bahnhofstrasse', value=320, mortgage=160, rent=(28, 150, 450, 1000, 1200, 1400), group=8, position=35, next_pos=36, rect=rects[34], houses=house200),
                 RealEstate(name='Hauptbahnhof', value=200, mortgage=100, rent=RealEstate.railway_rent, group=RealEstate.RAILWAY, position=36, next_pos=37, rect=rects[35], houses=None),
                 EventTile(position=37, next_pos=38, rect=rects[36], stack=event_stack),
                 RealEstate(name='Parkstrasse', value=350, mortgage=175, rent=(35, 175, 500, 1100, 1300, 1500), group=9, position=38, next_pos=39, rect=rects[37], houses=house200),
                 TaxTile(amount=100, position=39, next_pos=40, rect=rects[38]),
                 RealEstate(name='Schlossallee', value=400, mortgage=200, rent=(50, 200, 600, 1400, 1700, 2000), group=9, position=40, next_pos=1, rect=rects[39], houses=house200),
                 JailTile(position=41, next_pos=12, rect=rects[10]))
        return Game(tiles, 1500, players)

