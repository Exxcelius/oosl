import pickle
from _collections_abc import Iterator
import neat
from time import sleep

import pygame

import Assets
from Tiles import *
import os

path = os.path.dirname(__file__)
assets = os.path.join(path, 'Assets')

if __name__ == '__main__':
    from Project.Tiles import *


def read_bool():
    while True:
        user_input = input()
        try:
            m_bool = bool(user_input)
        except Exception:
            print("Wrong input")

        if m_bool is True or m_bool is False:
            print("Input accepted")
            return m_bool


class Player(pygame.sprite.DirtySprite):

    class Cash:
        def __init__(self, amount=0):
            self.amount = amount

        def __add__(self, other):
            if type(other) == type(self):
                self.amount = other.amount
            elif type(other) == int:
                self.amount += other
            if self.amount < 0:
                raise NotEnoughMoneyError()

        def __iadd__(self, other):
            self + other

        def __sub__(self, other):
            if type(self) == type(other):
                self += -other.amount
            else:
                self += -other

        def __isub__(self, other):
            self - other

    def __init__(self, cash=0, index=0, game=None):
        pygame.sprite.Sprite.__init__(self)

        self.game = game
        self.index = index
        self.position = 1
        self.target_position = 1

        self.image = Assets.figures[self.index - 1]
        self.rect = self.image.get_rect()

        self.dice = (0, 0)
        self.doublets = 0
        self.rounds_in_prison = 0
        self.cash = Player.Cash(cash)
        self.real_estate = []
        self.jail_free = 0
        self.cards = []
        self.finished_turn = False

    def __str__(self):
        out = "Player %d\n" % self.index
        out += "Cash: %d\n" % self.cash
        out += "Position:%d\n" % self.target_position
        if len(self.real_estate) != 0:
            out += "Real Estate:\n"
            for re in self.real_estate:
                out += str(re)
        return out

    def net_worth(self) -> int:
        worth = self.cash
        for re in self.real_estate:
            if not re.is_mortgaged:
                worth += re.value
                if re.houses is not None:
                    for house in re.houses:
                        worth += house.cost if house.is_build else 0
            else:
                worth -= re.mortgage * 1.1
        return worth

    def move(self, cur_dice):
        d1 = cur_dice[0]
        d2 = cur_dice[1]
        self.dice = (d1, d2)

        if d1 == d2:
            self.doublets += 1
        else:
            self.doublets = 0
        if self.doublets == 3:
            self.target_position = 41  # Jail

        if not self.position == 41:  # Player not in jail
            self.target_position = self.position + d1 + d2
            if self.target_position > 40:
                self.target_position -= 40

        else:  # Jail
            gets_free = self.pay_bail() or self.use_jail_free_card()
            if not gets_free and self.rounds_in_prison == 3:
                self._pay_bail(True)
                gets_free = True
            if gets_free or self.doublets != 0:
                self.target_position = 11 + d1 + d2  # jail_visit_tile + dice

    def update(self, do_graphics=True, *args, **kwargs):
        if self.position != self.target_position:
            print(self.position, self.target_position)
            cur_x, cur_y = self.game.get_player_position(self, self.position)

            if self.target_position == 41:  # Jail
                target_x, target_y = self.game.get_player_position(self, 41)

            else:
                next_tile = next(self.game.get_tile_by_position(self.position))
                target_x, target_y = self.game.get_player_position(self, next_tile)

            print('\t', cur_x, cur_y)
            print('\t', target_x, target_y)
            print('\t', self.rect.center)

            if self.rect.center != (target_x, target_y):
                # TODO implement nice, fluent movement
                if do_graphics:
                    sleep(0.1)
                self.rect.center = (target_x, target_y)

            else:
                self.position = self.position + 1 if self.target_position != 41 else 41
                self.game.get_tile_by_position(self.position).moved_over_tile(self)

                if self.position == self.target_position:
                    self.game.get_tile_by_position(self.position).landed_on_tile(self)
                    self.dice = (0, 0)
                    self.finished_turn = True

    def _buy_real_estate(self, real_estate: RealEstate, user_input: bool):
        if user_input:
            try:
                self.cash -= real_estate.value
                self.real_estate.append(real_estate)
                real_estate.owner = self
            except NotEnoughMoneyError:
                raise NotEnoughMoneyWarning

    def buy_real_estate(self, real_estate):
        user_input = read_bool()
        self._buy_real_estate(real_estate, user_input)

    def sell_real_estate(self, real_estate: RealEstate, user_input: bool):
        if real_estate.owner != self:
            return
        if user_input:
            if len([0 for house in real_estate.houses if house.is_build]) != 0 or real_estate.houses is None:
                return
            else:
                real_estate.owner = None
                self.real_estate.remove(real_estate)
                self.cash += real_estate.value

    def _pay_bail(self, user_input: bool):
        if user_input:
            self.cash -= 50
            return True
        return False

    def pay_bail(self):
        user_input = read_bool()  # TODO implement with GUI/CLI
        self._pay_bail(user_input)

    def use_jail_free_card(self):  # TODO implement with GUI/CLI
        user_input = read_bool()
        self._use_jail_free_card(user_input)

    def _use_jail_free_card(self, user_input: bool):
        if not user_input:
            return
        if len(self.cards) < 1:
            return

        card = self.cards.pop(0)
        card.use_card(self)

    def _build_houses(self, real_estate: RealEstate):
        if real_estate.owner != self:
            return
        if real_estate.houses is None:
            return

        owns_all_tiles = False
        group = [tile for tile in self.game.tiles if tile is RealEstate and Tile.group == real_estate.group]
        for tile in group:
            owns_all_tiles &= tile.owner == self

        if not owns_all_tiles:
            return

        build = []
        for tile in group:
            if tile != real_estate:
                build.append(tile.get_build)

        if min(build) < real_estate.get_build():
            return

        if len([0 for house in real_estate.houses if house.is_build]) >= len(real_estate.houses):
            return
        for house in real_estate.houses:
            if not house.is_build:
                try:
                    self.cash -= house.cost
                    house.is_build = True
                except NotEnoughMoneyError:
                    raise NotEnoughMoneyWarning
                break

    def sell_houses(self, real_estate):
        if self != real_estate.owner:
            return
        if real_estate.houses is None:
            return
        group = [tile for tile in self.game.tiles if type(tile).__name__ == 'RealEstate' and tile.group == real_estate.group]
        build = []
        for tile in group:
            if tile != real_estate:
                build.append(tile.get_build())

        if max(build) > real_estate.get_build():
            return

        for x, house in enumerate(real_estate.houses):
            if not house.is_build:
                if x != 0:
                    real_estate.houses[x - 1].is_build = False
                    self.cash += real_estate.houses[x - 1].cost / 2

    def mortgage_real_estate(self, real_estate):
        if real_estate.owner != self:
            return False
        if real_estate.is_mortgaged:
            return False
        if real_estate.houses is not None:
            if len([0 for building in real_estate.houses if building.is_build]) != 0:
                return False

        real_estate.is_mortgaged = True
        self.cash += real_estate.mortgage
        return True

    def de_mortgage_real_estate(self, real_estate: RealEstate):
        if not real_estate.is_mortgaged:
            return False
        if real_estate.owner != self:
            return False
        else:
            try:
                self.cash -= int(real_estate.mortgage * 1.1)
                real_estate.is_mortgaged = False
            except NotEnoughMoneyError:
                raise NotEnoughMoneyWarning

            return True


class AiPlayer(Player):

    def __init__(self, genome=None, config=None, *args, **kwargs):
        Player.__init__(self, *args, **kwargs)
        pygame.sprite.Sprite.__init__(self)

        if genome is None:
            try:
                with open(os.path.join(os.path.dirname(__file__), "ANN_Genome.bin")) as file:
                    self.genome = pickle.load(file)
            except RuntimeError as e:
                pass
        else:
            self.network = neat.nn.FeedForwardNetwork.create(genome, config)
            self.genome = genome
            self.genome.fitness = 0

    def activate_ann(self, position):
        tile = self.game.get_tile_by_position(position)
        value = 0 if type(tile).__name__ != 'RealEstate' else tile.value
        available = len([0 for tile in self.game.tiles if type(tile).__name__ == 'RealEstate' and tile.owner is None])
        group = [t for t in self.game.tiles if type(t).__name__ == 'RealEstate' and tile.group == t.group]
        available_in_group = len([0 for tile in group if tile.owner is None])
        owned_in_group = len([0 for t in group if t.owner == self])
        group_owners = len([0 for t in group if t.owner != self and t.owner is not None])
        jail_free = len([0 for card in self.cards if card is not None])

        ai_in = (self.cash, position, value, len(self.real_estate), available, group_owners,
                                       available_in_group, owned_in_group, jail_free)

        output = self.network.activate(ai_in)
        return output

    def buy_real_estate(self, real_estate):
        output = self.activate_ann(real_estate.position)
        self._buy_real_estate(real_estate, output[0] > 0.5)

    def build_houses(self, real_estate: RealEstate):
        output = self.activate_ann(real_estate.position)
        if output[1] > 0.5:
            self._build_houses(real_estate)

    def use_jail_free_card(self):
        output = self.activate_ann(41)
        self._use_jail_free_card(output[2] > 0.5)

    def pay_bail(self):
        output = self.activate_ann(41)
        self._pay_bail(output[3] > 0.5)

    def sell_real_estate(self, real_estate):
        output = self.activate_ann(real_estate.position)
        if output[4] > 0.5:
            Player.sell_real_estate(self, real_estate)

    def sell_houses(self, real_estate):
        output = self.activate_ann(real_estate.position)
        if output[5] > 0.5:
            Player.sell_houses(self, real_estate)

    def mortgage_real_estate(self, real_estate):
        output = self.activate_ann(real_estate.position)
        if output[6] > 0.5:
            Player.mortgage_real_estate(self, real_estate)

    def de_mortgage_real_estate(self, real_estate: RealEstate):
        output = self.activate_ann(real_estate.position)
        if output[7] > 0.5:
            Player.de_mortgage_real_estate(self, real_estate)


class NotEnoughMoneyError(RuntimeError):

    def __init__(self, creditor=None, *args, **kwargs):
        RuntimeError.__init__(self, *args, **kwargs)
        self.creditor = creditor


class NotEnoughMoneyWarning(UserWarning):
    pass
