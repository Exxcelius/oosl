import warnings


class ActionCard:

    def __init__(self, text, is_keepable=False, action=None, is_optiondialog=False):
        self.text = text
        self.is_keepable = is_keepable
        self.action = action
        self.stack = None
        self.is_optiondialog = is_optiondialog

    def update(self, player, *args, **kwargs):
        if self.action is None:
            warnings.warn("An Action Card should have an action implemented and supplied to its __init__")
        else:
            self.action(player, *args, **kwargs)
        if not self.is_keepable:
            self.stack.cards.append(self)
        else:
            player.cards.append(self)


class EventCard(ActionCard):
    pass


class CommunityCard(ActionCard):
    pass


class ActionCardStack:

    def __init__(self, cards, card_type):
        self.cards = []
        for c in cards:
            if isinstance(c, card_type):
                self.cards.append(c)
                c.stack = self

    def draw(self, player, *args, **kwargs)-> ActionCard:
        card = self.cards.pop(0)
        card.update(player, *args, **kwargs)
        return card


class EventCardStack(ActionCardStack):

    def __init__(self, cards):
        super().__init__(cards, EventCard)

    def draw(self, player, *args, **kwargs) -> EventCard:
        ActionCardStack.draw(player, *args, **kwargs)


class CommunityCardStack(ActionCardStack):

    def __init__(self, cards):
        super().__init__(cards, CommunityCard)


class ActionCardStackFactory:

    def __init__(self, cards, card_type=ActionCard):
        self.cards = cards
        self.card_type = card_type

    def create_stacks(self):
        return ActionCardStack(self.cards, self.card_type)


class EventCardStackFactory(ActionCardStackFactory):

    def create_stacks(self):
        return EventCardStack(self.cards)


class CommunityCardStackFactory(ActionCardStackFactory):

    def create_stacks(self):
        return CommunityCardStack(self.cards)


class ClassicGermanEventCardStackFactory(EventCardStackFactory):

    def __init__(self):
        self.card_type = EventCard
        # TODO implemented actions
        self.cards = (EventCard(text='Du bist zum Vorstand gewählt worden.\nZahle jedem Spieler\n50€'),
                      EventCard(text='Du kommst aus dem Gefägnis frei.\n\nDiese Karte musst du behalten,\bis du sie benötigst oder verkaufst.'),
                      EventCard(text='Strafe für zu schnelles Fahren:\n15€'),
                      EventCard(text='Rücke vor bis zur Seestrasse.\nWenn du über LOS kommst,\nziehe 200€ ein.'),
                      EventCard(text='Du hast in einem Kreuzworträtsel-\n wettbewerb gewonnen.\nZiehe 100€ ein'),
                      EventCard(text='Zahle Schulgeld:\n150€'),
                      EventCard(text='Die Bank zahlt dir eine Dividende:\n50€'),
                      EventCard(text='Gehe 3 Felder zurück.'),
                      EventCard(text='Zahle Schulgeld:\n150€'),
                      EventCard(text='Rücke vor bis zur\nSchlossallee'),
                      EventCard(text='Gehe in das Gefängnis!\nBegib dich direkt dorthin.\nGehe nicht über LOS.\nZiehe nicht 200€ ein.'),
                      EventCard(text='Lasse alle deine Häuser renovieren!\nZahle an die Bank:\nfür jedes Haus 25€\nfür jedes Hotel 100€'),
                      EventCard(text='Miete und Anleihzinsen\nwerden fällig. Die Bank zahlt dir:\n150€'),
                      EventCard(text='Mache einen Ausflug zum Südbahnhof.\nWenn du über LOS kommst,\nziehe 200€ ein.'),
                      EventCard(text='Rücke vor bis auf LOS'),
                      EventCard(text='Du wirst zu Strassenausbesserungs-\narbeiten herangezogen.\nZahle für deine Häuser und Hotels:\n40€ je Haus\n115€ je Hotel an die Bank')
                      )


class ClassicGermanCommunityCardStackFactory(CommunityCardStackFactory):

    def __init__(self):
        self.card_type = CommunityCard
        # TODO implemented actions
        self.cards = (CommunityCard(text='Zahle eine Strafe von 10€\nodernimm eine Ereigniskarte.', is_optiondialogue=True),
                      CommunityCard(text='Gehe in das Gefängnis!\nBegib dich direkt dorthin.\nGehe nicht über LOS.\nZiehe nicht 200€ ein.'),
                      CommunityCard(text='Du kommst aus dem Gefägnis frei.\n\nDiese Karte musst du behalten,\bis du sie benötigst oder verkaufst.'),
                      CommunityCard(text='Zahle am das Krankenhaus:\n100€'),
                      CommunityCard(text='Arzt-Kosten zahle:\n50€'),
                      CommunityCard(text='Du erhälst auf\nVorzugs-Aktien 7% Dividende:\n25€'),
                      CommunityCard(text='Du hast den 2.Preis in einer\nSchönheitskonkurrenz gewonnen.\nZiehe 10€ ein'),
                      CommunityCard(text='Du erbst:\n100€'),
                      CommunityCard(text='Aus Lagerverkäufen erhälst du:\n 50€'),
                      CommunityCard(text='Rücke vor bis zum nächsten Bahnhof.\nDer Eigentümer erhält das doppelte der normalen\nMiete. Hat noch kein Spieler einen\nBesitzanspruch auf diesen Bahnhof,\nso kannst du ihn von der Bank kaufen.'),
                      CommunityCard(text='Gehe zurück zur\nBadstrasse'),
                      CommunityCard(text='Die Jahresrente wird fällig.\nZiehe 100€ ein.'),
                      CommunityCard(text='Bank-Irrtum zu deinen Gunsten.\nZiehe 200€ ein.'),
                      CommunityCard(text='Einkommensteuer-Rückzahlung:\nZiehe 20€ ein.'),
                      CommunityCard(text='Es ist dein Geburstag.\nZiehe von jedem Spieler\n10€ ein.'),
                      CommunityCard(text='Rücke vor bis auf LOS')
                      )

