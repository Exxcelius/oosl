from _collections_abc import Iterator
import random

import pygame


def add_items(i_list, items=None):
    if items is not None:
        try:
            m_iter = iter(items)
            for item in m_iter:
                i_list.append(item)
        except Exception:
            i_list.append(items)


def text_objects(text, font, color=(0, 0, 0)):
    text_surface = font.render(text, True, color)
    return text_surface, text_surface.get_rect()


def dice():
    return random.randint(1, 6), random.randint(1, 6)


class MyCycle(Iterator):
    def __init__(self, items):
        self.items = []
        add_items(self.items, items)
        self.index = 0

    def __next__(self):
        if len(self.items) == 0:
            raise StopIteration()

        if self.index >= len(self.items):
            self.index = 0

        out = self.items[self.index]
        self.index += 1
        return out

    def remove(self, item):
        self.items.remove(item)

    def append(self, item):
        self.items.append(item)


class Frame(pygame.sprite.Sprite):

    def __init__(self, rect: pygame.Rect, color=(0, 0, 0), layer=0):
        self.rect = rect
        self.color = color
        self.layer = layer
        self.visible = True


class Dialog(Frame):
    pygame.font.init()
    default_font = pygame.font.SysFont("Consolas", 30)

    def __init__(self, title="", text="", *args, **kwargs):
        Frame.__init__(self, *args, **kwargs)
        self.title = title
        self.text = text
        self.font = Dialog.default_font
        self.confirm_bt = pygame.Rect(0, 0, self.rect.width * 0.25, self.rect.height * 0.125)
        self.confirm_bt.center = (self.rect.centerx, self.rect.y + self.rect.height - self.confirm_bt.height)

    def draw(self, win):
        if self.visible:
            # draw main dialog body with border
            pygame.draw.rect(win, (255, 255, 255), self.rect)
            pygame.draw.rect(win, self.color, pygame.Rect(self.rect.x + self.rect.width * 0.05, self.rect.y * self.rect.width,
                                              self.rect.width * 0.9, self.rect.height - self.rect.width * 0.1))

            # draw title
            text_surf, text_rect = text_objects(self.title, self.font, self.color)
            text_rect.center = (self.rect.centerx, self.rect.height * 0.15)
            win.blit(text_surf, text_rect)

            # draw text line wise
            strings = self.text.split("\n")
            for x, string in enumerate(strings):
                text_surf, text_rect = text_objects(string, self.font, self.color)
                text_rect.centerx = self.rect.centerx
                text_rect.centery = self.rect.height * 0.4 + self.rect.height * 0.5 / len(strings) * x
                win.blit(text_surf, text_rect)

            # draw confirmation button
            pygame.draw.rect(win, self.color, self.confirm_bt, 1 if self.rect.width < 100 else int(self.rect.width * 0.01))
            ok_surf, ok_rect = text_objects("OK", self.font, self.color)
            ok_rect.center = self.confirm_bt.center
            win.blit(ok_surf, ok_rect)

    def on_clicked(self, x, y):
        if self.confirm_bt.collidepoint(x, y):
            self.visible = False


class YNDialog(Frame):
    pygame.font.init()
    default_font = pygame.font.SysFont("Consolas", 30)

    def __init__(self, title="", text="", *args, **kwargs):
        Frame.__init__(self, *args, **kwargs)
        self.click_result = None
        self.title = title
        self.text = text
        self.font = Dialog.default_font
        self.confirm_bt = pygame.Rect(0, 0, self.rect.width * 0.25, self.rect.height * 0.125)
        self.confirm_bt.center = (self.rect.centerx + self.rect.width * 0.25, self.rect.y + self.rect.height - self.confirm_bt.height)
        self.deny_bt = pygame.Rect(0, 0, self.rect.width * 0.25, self.rect.height * 0.125)
        self.deny_bt.center = (self.rect.centerx - self.rect.width * 0.25, self.rect.y + self.rect.height - self.confirm_bt.height)

    def draw(self, win):
        if self.visible:
            # draw main dialog body with border
            pygame.draw.rect(win, (255, 255, 255), self.rect)
            pygame.draw.rect(win, self.color,
                             pygame.Rect(self.rect.x + self.rect.width * 0.05, self.rect.y * self.rect.width,
                                         self.rect.width * 0.9, self.rect.height - self.rect.width * 0.1),
                             1 if self.rect.width < 100 else int(self.rect.width * 0.01))

            # draw title
            text_surf, text_rect = text_objects(self.title, self.font, self.color)
            text_rect.center = (self.rect.centerx, self.rect.height * 0.15)
            win.blit(text_surf, text_rect)

            # draw text line wise
            strings = self.text.split("\n")
            for x, string in enumerate(strings):
                text_surf, text_rect = text_objects(string, self.font, self.color)
                text_rect.centerx = self.rect.centerx
                text_rect.centery = self.rect.height * 0.4 + self.rect.height * 0.5 / len(strings) * x
                win.blit(text_surf, text_rect)

            # draw confirmation button
            pygame.draw.rect(win, self.color, self.confirm_bt.rect)
            ok_surf, ok_rect = text_objects("Yes", self.font, self.color, 1 if self.rect.width < 100 else int(self.rect.width * 0.01))
            ok_rect.center = self.confirm_bt.center
            win.blit(ok_surf, ok_rect)

            # draw deny button
            pygame.draw.rect(win, self.color, self.deny_bt.rect)
            no_surf, no_rect = text_objects("No", self.font, self.color, 1 if self.rect.width < 100 else int(self.rect.width * 0.01))
            no_rect.center = self.deny_bt.center
            win.blit(no_surf, no_rect)

    def on_clicked(self, x, y):
        if self.confirm_bt.collidepoint(x, y):
            self.click_result = True
            self.visible = False
        elif self.deny_bt.collidepoint(x, y):
            self.click_result = False
            self.visible = False


class FrameList:
    def __init__(self):
        self.items = []

    def add(self, items):
        add_items(self.items, items)

    def collide_point(self, x, y):
        """
        Checks if given point collides with one of its items, calls corresponding callback
        Only the item's callback with the highest layer will be called
        If multiple collisions on items with the same layer number occur, every of their callbacks will be called
        :param x: x part of target coordinate
        :param y: y part of target coordinate
        :return: bool -> coordinate is positioned on one of self's items
        """

        rel = []
        high_layer = 0
        for item in self.items:
            if item.layer > high_layer:
                high_layer = item.layer
                rel = [item]
            elif item.layer == high_layer:
                rel.append(item)

        for item in rel:
            item.on_clicked(x, y)

        return len(rel) > 0

    def draw(self, win: pygame.Surface):
        """
        Draw each contained item
        :param win: Surface to be drawn onto
        :return: None
        """
        for item in self.items:
            item.draw(win)

    def clear(self):
        self.items = []