import click
import csv
import os
import time

import requests

list_file_path = '/home/nils/Uni/SemIV/OOSL/Recorder/list.csv'


def list_records(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return

    with open(list_file_path, 'r') as recordings:
        recording_reader = csv.reader(recordings, delimiter=',')
        click.echo('Date'.ljust(11) + 'Time'.ljust(9) + ('Duration' + ' ').rjust(9) + 'Filepath'.ljust(48) + 'URL')
        for row in recording_reader:
            click.echo(row[0].ljust(11) + row[1].ljust(9) + (row[2] + ' ').rjust(9) + row[3].ljust(48) + row[4])

    ctx.exit()


@click.command()
@click.argument('url', required=True)
@click.option('-d', '--duration', type=int, default=20)
@click.option('-f', '--fileName', default='MyRecord.mp3')
@click.option('-l', '--list', is_flag=True, callback=list_records, expose_value=False, is_eager=True)
def record_audio(url, duration, filename):
    print("Recording audio...")
    m_file = open(filename, 'wb')
    chunk_size = 1024

    start_time_in_seconds = time.time()

    time_limit = duration
    time_elapsed = 0
    with requests.Session() as session:
        response = session.get(url, stream=True)
        for chunk in response.iter_content(chunk_size=chunk_size):
            if time_elapsed > time_limit:
                break
            # to print time elapsed
            if int(time.time() - start_time_in_seconds) - time_elapsed > 0:
                time_elapsed = int(time.time() - start_time_in_seconds)
                print(time_elapsed, end='\r', flush=True)
            if chunk:
                m_file.write(chunk)

        m_file.close()

    with open(list_file_path, 'a') as recordings:
        recording_writer = csv.writer(recordings, delimiter=',', quotechar='"')
        recording_writer.writerow([time.strftime("%Y-%m-%d"), time.strftime("%H:%M:%S", time.localtime()),
                                   str(duration), os.getcwd() + '/' + filename, url])


if __name__ == '__main__':
    record_audio()
