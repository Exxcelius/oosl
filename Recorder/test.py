import click


def list_records(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    click.echo('listing')
    ctx.exit()


@click.command()
@click.argument('url', required=True)
@click.option('-d', '--duration', type=int, default=20)
@click.option('-f', '--fileName', default='MyRecord.mp3')
@click.option('-l', '--list', is_flag=True, callback=list_records, expose_value=False, is_eager=True)
def record_audio(url, duration, filename):
    click.echo('recording')


if __name__ == '__main__':
    record_audio()
