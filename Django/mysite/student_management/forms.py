from django import forms
from .models import Student


class NewStudentForm(forms.Form):
    first_name = forms.CharField(max_length=50)
    sur_name = forms.CharField(max_length=50)
    mat_number = forms.CharField(max_length=10)

    def clean_mat_number(self):
        print(self.cleaned_data)
        print('Cleaning Mat')
        nr = self.cleaned_data['mat_number']
        if not nr.isnumeric:
            print('Mat contains characters')  # Error
        else:
            if len(Student.objects.filter(mat_number=nr)) != 0:
                print('Mat not unique')  # Error
            else:
                return nr


class ChangeStudentForm(forms.Form):
    first_name = forms.CharField(max_length=50, strip=True)
    sur_name = forms.CharField(max_length=50, strip=True)
    mat_number = forms.CharField(max_length=10, disabled=False)


class LessonSignUpForm(forms.Form):
    mat_number = forms.CharField(max_length=10)

    def clean_mat_number(self):
        print(self.cleaned_data)
        print('Cleaning Mat')
        nr = self.cleaned_data['mat_number']
        if not nr.isnumeric:
            print('Mat contains characters')  # Error
        else:
            if len(Student.objects.filter(mat_number=nr)) == 0:
                print('Mat does not exist')  # Error
            else:
                return nr
