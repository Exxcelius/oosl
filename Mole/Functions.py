import pygame as pg
import time


def back_button(window, color, center, radius):
    x, y = center[0], center[1]
    pg.draw.circle(window, color, center, radius - 1)
    pg.draw.circle(window, Color.BLACK, center, radius, 5)
    pg.draw.polygon(window, Color.BLACK, ((x - radius * 0.75, y), (x - radius * 0.2, y + radius * 0.6),
                                          (x - radius * 0.2, y + radius * 0.3),
                                          (x + radius * 0.65, y + radius * 0.3),
                                          (x + radius * 0.65, y - radius * 0.3),
                                          (x - radius * 0.2, y - radius * 0.3),
                                          (x - radius * 0.2, y - radius * 0.6),))


def text_objects(text, font, color=(0, 0, 0)):
    textSurface = font.render(text, True, color)
    return textSurface, textSurface.get_rect()


class Button(pg.Rect):

    def __callback(self, func=None):
        def wrapper(*args, **kwargs):
            print(f'Button {self.label} pressed.')
            if func:
                func(*args, **kwargs)

        return wrapper

    def __init__(self, rect, label, callback=None, is_active=True):
        super().__init__(rect)
        self.rect = pg.Rect(rect)
        self.label = label
        self.callback = self.__callback(callback) if callback else self.__callback()
        self.font = pg.font.Font('freesansbold.ttf', int(self.rect.height * 0.87))
        self.is_active = is_active

    def draw(self, surf, mouse):
        mouse_x, mouse_y = mouse[0], mouse[1]

        color = Color.GREEN_GRAY if not self.is_active else \
            Color.GREEN_BRIGHT if self.rect.collidepoint(mouse_x, mouse_y) else \
                Color.GREEN_DARK

        pg.draw.rect(surf, color, self.rect)
        pg.draw.rect(surf, Color.BLACK, self.rect, 5)

        if self.label != '':
            label_surf, label_rect = text_objects(self.label, self.font)
            label_rect.center = self.rect.center
            surf.blit(label_surf, label_rect)


class Setting(pg.Rect):
    def __init__(self, rect, labels, start_val=0, allows_overflow=False, is_active=True):
        super().__init__(rect)
        self.rect = pg.Rect(rect)
        self.labels = labels
        self.val = start_val
        self.allows_overflow = allows_overflow
        self.is_active = is_active
        self.font = pg.font.Font('freesansbold.ttf', int(self.rect.height * 0.87))

    def draw(self, surf):
        pg.draw.rect(surf, Color.GREEN if self.is_active else Color.GREEN_GRAY, self.rect)
        pg.draw.rect(surf, Color.BLACK, self.rect, 5)

        if self.labels[self.val] != '':
            label_surf, label_rect = text_objects(self.labels[self.val], self.font)
            label_rect.center = self.rect.center
            surf.blit(label_surf, label_rect)

        if self.val > 0 or self.allows_overflow:
            pg.draw.polygon(surf, Color.BLACK, ((self.rect.width * -0.425 + self.rect.centerx,
                                                 self.rect.height * -0.3 + self.rect.centery),
                                                (self.rect.width * -0.48125 + self.rect.centerx,
                                                 self.rect.centery),
                                                (self.rect.width * -0.425 + self.rect.centerx,
                                                 self.rect.height * 0.3 + self.rect.centery)))

        if self.val < len(self.labels) - 1 or self.allows_overflow:
            pg.draw.polygon(surf, Color.BLACK,
                            ((self.rect.width * 0.425 + self.rect.centerx, self.rect.height * -0.3 + self.rect.centery),
                             (self.rect.width * 0.48125 + self.rect.centerx, self.rect.centery),
                             (self.rect.width * 0.425 + self.rect.centerx, self.rect.height * 0.3 + self.rect.centery)))

    def check_collision(self, p_x, p_y):
        if self.rect.width * -0.48125 + self.rect.centerx <= p_x <= self.rect.width * -0.425 + self.rect.centerx \
                and self.rect.height * -0.3 + self.rect.centery <= p_y <= self.rect.height * 0.3 + self.rect.centery:
            if self.allows_overflow:
                self.val = self.val - 1 if self.val >= 1 else len(self.labels) - 1
            else:
                self.val = self.val - 1 if self.val >= 1 else self.val

        if self.rect.width * 0.425 + self.rect.centerx <= p_x <= self.rect.width * 0.48125 + self.rect.centerx \
                and self.rect.height * -0.3 + self.rect.centery <= p_y <= self.rect.height * 0.3 + self.rect.centery:
            if self.allows_overflow:
                self.val = self.val + 1 if self.val < len(self.labels) - 1 else 0
            else:
                self.val = self.val + 1 if self.val < len(self.labels) - 1 else self.val


class Color:
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    GREEN_BRIGHT = (15, 153, 41)
    GREEN = (4, 105, 23)
    GREEN_DARK = (2, 66, 14)
    GREEN_GRAY = (70, 115, 78)


class Highscore:

    def __init__(self, gamemode, name, score, date):
        """
        :param gamemode: played gamemode
        :param name: name of the player
        :param score: achieved score
        :param date: formatted time str '%Y/%m/%d'
        """
        self.mode = gamemode
        self.name = name
        self.score = score
        self.date = date

    def __repr__(self):
        return '{mode:%s;name:%s;score:%s;date:%s}' % (self.mode, self.name, self.score, self.date)
