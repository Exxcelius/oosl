import os
import random
import re
import sys
import time

import pygame
from pygame.locals import *

from Functions import *
from rounded_rect import *

pygame.init()
fpsClock = pygame.time.Clock()
fps = 30

win_x, win_y = int(1920 * 0.95), int(1080 * 0.9)  # TODO get User Sreen Size
window = pygame.display.set_mode((win_x, win_y))

pygame.display.set_caption("Slash the Slime")
pygame.mouse.set_cursor((8, 8), (0, 0), (0, 0, 0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0, 0, 0))  # Hide cursor

# Path variables
game_folder = os.path.dirname(__file__)
asset_folder = os.path.join(game_folder, 'Assets')
image_folder = os.path.join(asset_folder, 'Images')
highscore_file_path = os.path.join(game_folder, 'score.data')

# Fonts
settings_text = pygame.font.Font('freesansbold.ttf', int(win_y * 0.0375))
high_score_text = pygame.font.Font(os.path.join(game_folder, 'SpaceMono-Regular.ttf'), int(win_y * 0.0375))
largeText = pygame.font.Font('freesansbold.ttf', int(win_y * 0.1))
mediumText = pygame.font.Font('freesansbold.ttf', int(win_y * 0.075))

# Game state variables
is_debug_mode = False  # Enables console printing
is_main_menu = True
is_pause_menu = False
is_stat_menu = False
is_highscore = False
is_settings_menu = False
is_credits = False
is_running = False
is_end_screen = False
is_writing_highscore_name = False

run = True
highscore_mode = 0

# longest possible name with largest charachters is 9 charackters
highscore_name = 'Slimy'
max_chars = 13

# Settings variables
game_mode_titles = ['Classic', 'Limited Stamina', 'Sniper', 'Deathmatch']
game_mode = 1


frequency_titles = ['Spring water', 'Active Filtering', "Townside", "Metropolis", 'Chemical Plant']
frequency_prob = [0.005, 0.01, 0.02, 0.025, 0.04]
frequency_mode = len(frequency_titles) // 2

speed_titles = ["Teatime", "Slow", "Normal", "Ultra", "Hypersonic"]
speed_times = [fps * 3, fps * 1.7, fps * 1.3, fps * 0.1, fps * 0.75]
speed_mode = len(speed_titles) // 2

grid_titles = ["Tiny", "Small", "Normal", "Large", "Humongous"]
grid_sizes = [(13, 13), (9, 9), (5, 5), (4, 4), (3, 3)]
grid_mode = len(grid_titles) // 2

# button objects
main_power = pygame.Rect(win_x * 0.01, win_x * 0.01, win_x * 0.05, win_x * 0.05)
main_settings = pygame.Rect(win_x * 0.065, win_x * 0.01, win_x * 0.045, win_x * 0.045)
main_title = pygame.Rect(win_x * 0.27, win_y * 0.23, win_x * 0.46, win_y * 0.12)
main_play = Button((win_x * 0.3, win_y * 0.39, win_x * 0.4, win_y * 0.1), 'Play')
main_highscore = Button((win_x * 0.3, win_y * 0.5, win_x * 0.4, win_y * 0.1), "Hall o' Fame")
main_credits = Button((win_x * 0.3, win_y * 0.61, win_x * 0.4, win_y * 0.1), 'Credits')

settings_mode = Setting((win_x * 0.325, win_y * 0.325, win_x * 0.35, win_y * 0.05), game_mode_titles, game_mode, True)
settings_grid = Setting((win_x * 0.325, win_y * 0.475, win_x * 0.35, win_y * 0.05), grid_titles, grid_mode)
settings_speed = Setting((win_x * 0.325, win_y * 0.625, win_x * 0.35, win_y * 0.05), speed_titles, speed_mode)
settings_frequenzy = Setting((win_x * 0.325, win_y * 0.775, win_x * 0.35, win_y * 0.05), frequency_titles, frequency_mode)

highscore_select = Setting((win_x * 0.35, win_y * 0.3125, win_x * 0.3, win_y * 0.05), game_mode_titles, highscore_mode, True)

pause_continue = Button((win_x * 0.35, win_y * 0.325, win_x * 0.3, win_y * 0.1), "Keep hittin'")
pause_stats = Button((win_x * 0.35, win_y * 0.5, win_x * 0.3, win_y * 0.1), 'Status')
pause_exit = Button((win_x * 0.35, win_y * 0.675, win_x * 0.3, win_y * 0.1), 'Give Up!')

end_name_field = Button((win_x * 0.35, win_y * 0.435, win_x * 0.3, win_y * 0.1), '')
end_submit = Button((win_x * 0.35, win_y * 0.5725, win_x * 0.3, win_y * 0.1), 'Submit')
end_cancel = Button((win_x * 0.35, win_y * 0.71, win_x * 0.3, win_y * 0.1), 'Go Back')

end_text_line = Button((win_x * 0.36, win_y * 0.435 + win_x * 0.01, win_x * 0.28, win_y * 0.1 - win_x * 0.02), '')
end_text_line_border = pygame.Rect(win_x * 0.36 - 2, win_y * 0.435 + win_x * 0.01 - 2, win_x * 0.28 + 4,
                                   win_y * 0.1 - win_x * 0.02 + 4)

# Assets
hammer_up = pygame.image.load(os.path.join(image_folder, 'mace_up.png')).convert()
hammer_up.set_colorkey((255, 255, 255))
hammer_up = pygame.transform.scale(hammer_up, (int(win_x * 0.065), int(win_x * 0.065)))
hammer_down = pygame.image.load(os.path.join(image_folder, 'maze_down.png')).convert()
hammer_down.set_colorkey((255, 255, 255))
hammer_down = pygame.transform.scale(hammer_down, (int(win_x * 0.065), int(win_x * 0.065)))

anim_duration = 36
slime_path = os.path.join(image_folder, 'Slimes')
anims = {0: [pygame.image.load(os.path.join(slime_path, 'monster_02_C_idle.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_C_attack_01.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_C_attack_02.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_C_attack_03.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_C_attack_02.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_C_attack_01.png')).convert()],
         1: [pygame.image.load(os.path.join(slime_path, 'monster_02_B_idle.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_B_attack_01.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_B_attack_02.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_B_attack_03.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_B_attack_02.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_B_attack_01.png')).convert()],
         2: [pygame.image.load(os.path.join(slime_path, 'monster_02_A_idle.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_A_attack_01.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_A_attack_02.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_A_attack_03.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_A_attack_02.png')).convert(),
             pygame.image.load(os.path.join(slime_path, 'monster_02_A_attack_01.png')).convert()]}

ko_folder = os.path.join(image_folder, 'Slimes_ko')
creature_hit_anim_duration = 22
ko_anim = [pygame.image.load(os.path.join(ko_folder, 'frame_00_delay-0.1s.gif')).convert(),
           pygame.image.load(os.path.join(ko_folder, 'frame_01_delay-0.1s.gif')).convert(),
           pygame.image.load(os.path.join(ko_folder, 'frame_02_delay-0.1s.gif')).convert(),
           pygame.image.load(os.path.join(ko_folder, 'frame_03_delay-0.1s.gif')).convert(),
           pygame.image.load(os.path.join(ko_folder, 'frame_04_delay-0.1s.gif')).convert(),
           pygame.image.load(os.path.join(ko_folder, 'frame_05_delay-0.1s.gif')).convert(),
           pygame.image.load(os.path.join(ko_folder, 'frame_06_delay-0.1s.gif')).convert(),
           pygame.image.load(os.path.join(ko_folder, 'frame_07_delay-0.1s.gif')).convert(),
           pygame.image.load(os.path.join(ko_folder, 'frame_08_delay-0.1s.gif')).convert(),
           pygame.image.load(os.path.join(ko_folder, 'frame_09_delay-0.1s.gif')).convert(),
           pygame.image.load(os.path.join(ko_folder, 'frame_10_delay-0.1s.gif')).convert()]

for img in ko_anim:
    img.set_colorkey((255, 255, 255))

# ingame variabless
total_clicks = 0
hits = 0
score = 0

score_mult = 10
score_mult_inv = 1 / (score_mult / speed_times[speed_mode] * fps / frequency_prob[frequency_mode] / 10)
score_mult_text = 1

is_hitting = False

last_time = None  # to set at each frame if is_running or is_pause_menu
passed_time = 0
max_time = 45

max_clicks = 50

mouse_x, mouse_y = 0, 0

CREATURE_HIT_EVENT = USEREVENT + 1


def get_cur_multiplier():
    return score_mult / speed_times[speed_mode] * fps / frequency_prob[frequency_mode] * 0.1


def echo(message, ignore_debug=False):
    if is_debug_mode or ignore_debug:
        print(message)


class Anim(pygame.sprite.Sprite):

    def __init__(self, pos, width, height, images, duration, color_key=(255, 255, 255)):
        pygame.sprite.Sprite.__init__(self)
        self.images = images
        self.state = 0
        self.duration = duration
        self.rect = pygame.rect.Rect(0, 0, width, height)
        self.dim = (width, height)
        self.rect.center = pos

        for i in range(len(self.images)):
            self.images[i].set_colorkey(color_key)
            self.images[i] = pygame.transform.scale(self.images[i], self.dim)

        self.image = self.images[0]

    def update(self, *args):
        if len(self.images) > 1:
            self.state = self.state + 1 if self.state < self.duration - 1 else 0
            self.image = self.images[int(self.state / self.duration * len(self.images))]


class CreatureArray:
    def __init__(self, width, height, lifetime, ko_lifetime, propability):
        self.width = width
        self.height = height
        self.arr = [[None for x in range(width)] for y in range(height)]
        self.sprites = pygame.sprite.Group()
        self.creature_lifetime = lifetime
        self.creature_ko_lifetime = ko_lifetime
        self.creature_propability = propability

    def __str__(self):
        out = ''
        for row in self.arr:
            out += str(row) + '\n'
        return out

    def update(self):
        for i in range(len(self.arr)):
            for j in range(len(self.arr[0])):
                creature = self.arr[i][j]
                if creature is None:
                    rand = random.random()
                    if rand < self.creature_propability:
                        self.arr[i][j] = Creature(pos=(win_x * 0.2 - win_x * 0.2 / grid_sizes[grid_mode][0] + win_x * 0.75 / self.width * i,
                                                       win_y * 0.225 + win_y * 0.775 / self.height * j),
                                                  width=win_x * 0.4 / grid_sizes[grid_mode][0],
                                                  height=win_x * 0.4 / grid_sizes[grid_mode][0],
                                                  lifetime=self.creature_lifetime,
                                                  ko_lifetime=self.creature_ko_lifetime)
                        self.sprites.add(self.arr[i][j].anim)
                else:
                    if creature.lifetime < 0:
                        echo((i, j, 'Lifetime over'))
                        self.sprites.remove(creature.ko_anim if creature.is_hit else creature.anim)
                        self.arr[i][j] = None
                        del creature
                    else:
                        creature.update()

        self.sprites.update()

    def draw(self, surf):
        self.sprites.draw(surf)

    def check_hit(self, mouse):
        for row in self.arr:
            for creature in row:
                if creature is not None:
                    creature.check_hit(mouse)


class Creature:
    types = {0: 0.1, 1: 0.35, 2: 1.0}

    def __init__(self, pos, width, height, lifetime, ko_lifetime):
        rand = random.random()
        for key in Creature.types.keys():
            if rand <= Creature.types[key]:
                self.type = key
                break

        self.is_hit = False

        self.anim = Anim(pos, int(width), int(height), anims[self.type], anim_duration, color_key=(0, 0, 255))
        self.ko_anim = Anim(pos, int(width), int(height), ko_anim, creature_hit_anim_duration)
        self.rect = pygame.rect.Rect(0, 0, width, height)
        self.pos = pos
        self.rect.center = pos
        self.lifetime = lifetime
        self.ko_lifetime = ko_lifetime

    def __str__(self):
        return f'{super().__str__()} ({self.type})({self.is_hit})'

    def update(self):
        if not self.is_hit:
            self.anim.update()
        self.lifetime -= 1

    def check_hit(self, mouse):
        if self.rect.collidepoint(mouse[0], mouse[1]):
            if not self.is_hit:
                self.is_hit = True
                self.lifetime = self.ko_lifetime
                pygame.event.post(pygame.event.Event(CREATURE_HIT_EVENT, message=self))


bg = pygame.sprite.Group()
bg.add(Anim((win_x * 0.5, win_y * 0.5), win_x, win_y, [pygame.image.load(os.path.join(image_folder, 'Sewer01.jpg')).convert()], 60))

# read previous Highscores
highscores = {x: [] for x in range(len(game_mode_titles))}

try:
    with open(highscore_file_path, 'r') as f:
        for line in f.readlines():
            line = re.sub('[{}\n]', '', line)
            items = re.split(';', line)
            values = {'mode': 0, 'name': '', 'score': 0}
            for item in items:
                temp = re.split(':', item)
                values[temp[0]] = temp[1]
            highscores[int(values['mode'])].append(Highscore(values['mode'], values['name'],
                                                             values['score'], values['date']))
except FileNotFoundError:
    echo('No Highscore file found', ignore_debug=True)

# Main Game Cycle
while run:

    # update
    if is_running:
        # calculate new multiplier
        score_mult = score_mult - 0.125 / fps if score_mult >= 0.125 / fps else 0
        score_mult_text = get_cur_multiplier() * score_mult_inv

        # calculate time diff
        cur_time = int(time.time())
        passed_time += cur_time - last_time
        last_time = cur_time

        if game_mode == 3:  # Deathmatch Gamemode
            if passed_time > max_time:
                is_running = False
                is_end_screen = True

        creature_arr.update()

    elif is_pause_menu:
        last_time = int(time.time())

    # Event Handling
    for event in pygame.event.get():
        if event.type == QUIT:
            with open(highscore_file_path, 'w') as f:
                for cat in highscores:
                    for item in highscores[cat]:
                        f.write(str(item) + '\n')
            pygame.quit()
            run = False
            sys.exit(0)

        elif event.type == MOUSEMOTION:
            mouse_x, mouse_y = event.pos

        elif event.type == MOUSEBUTTONDOWN:
            mouse_x, mouse_y = event.pos
            is_hitting = True

            if is_main_menu:
                if is_settings_menu:

                    # setting arrows
                    settings_mode.check_collision(mouse_x, mouse_y)
                    settings_speed.check_collision(mouse_x, mouse_y)
                    settings_grid.check_collision(mouse_x, mouse_y)
                    settings_frequenzy.check_collision(mouse_x, mouse_y)

                    # Back Button
                    if win_x * 0.01 <= mouse_x <= win_x * 0.06 and win_y * 0.01 <= mouse_y <= win_y * 0.01 + win_x * 0.05:
                        is_settings_menu = False
                        echo('Exiting Settings from Exit Button')

                elif is_credits:
                    if win_x * 0.01 <= mouse_x <= win_x * 0.06 and \
                            win_y * 0.01 <= mouse_y <= win_y * 0.01 + win_x * 0.05:
                        is_credits = False
                        echo('Back Button (Credits) pressed.')

                elif is_highscore:
                    if win_x * 0.01 <= mouse_x <= win_x * 0.06 \
                            and win_y * 0.01 <= mouse_y <= win_y * 0.01 + win_x * 0.05:
                        is_highscore = False
                        echo('Back Button (Credits) pressed.')

                    highscore_select.check_collision(mouse_x, mouse_y)
                    highscore_mode = highscore_select.val

                else:  # Main Menu
                    # Main Power Button
                    if main_power.collidepoint(mouse_x, mouse_y):
                        echo("Exiting from Main Menu Power Button")
                        pygame.event.post(pygame.event.Event(QUIT))

                    # Play Button
                    elif main_play.collidepoint(mouse_x, mouse_y):
                        main_play.callback()
                        game_mode = settings_mode.val
                        grid_mode = settings_grid.val
                        speed_mode = settings_speed.val
                        frequency_mode = settings_frequenzy.val
                        is_main_menu = False
                        is_running = True
                        creature_arr = CreatureArray(grid_sizes[grid_mode][0], grid_sizes[grid_mode][1],
                                                     speed_times[speed_mode], creature_hit_anim_duration - 1,
                                                     frequency_prob[frequency_mode])
                        score_mult_inv = 1 / (score_mult / speed_times[speed_mode] * fps
                                              / frequency_prob[frequency_mode] / 10)
                        last_time = int(time.time())
                        echo(f'Starting Play Mode\n\tSpeedmode: {speed_mode}\n\tGridsize: {grid_sizes[grid_mode]}')

                    # Highscore Button
                    elif main_highscore.collidepoint(mouse_x, mouse_y):
                        main_highscore.callback()
                        is_highscore = True

                    # Credits Button
                    elif main_credits.collidepoint(mouse_x, mouse_y):
                        main_credits.callback()
                        is_credits = True

                    # Settings Button
                    elif main_settings.collidepoint(mouse_x, mouse_y):
                        echo('Entering Settings Menu')
                        is_settings_menu = True

            elif is_running:
                # Testing for Mole Hit
                total_clicks += 1
                creature_arr.check_hit((mouse_x, mouse_y))
                score_mult -= 1

                # Testing for game end
                if game_mode == 0:  # Classic Gamemode
                    if total_clicks >= 10 and hits / total_clicks <= 0.5:
                        is_running = False
                        is_end_screen = True

                if game_mode == 1:  # Limited Stamina Gamemode
                    if total_clicks >= max_clicks:
                        is_running = False
                        is_end_screen = True

                if game_mode == 2:  # Sniper Gamemode
                    if total_clicks >= 10 and hits / total_clicks <= 0.8:
                        is_running = False
                        is_end_screen = True

            elif is_pause_menu:
                # Continuing
                if win_x * 0.01 <= mouse_x <= win_x * 0.06 and win_y * 0.01 <= mouse_y <= win_y * 0.01 + win_x * 0.05:
                    is_pause_menu = False
                    is_running = True
                    echo('Exiting Pause mode from Back Button')
                if pause_continue.collidepoint(mouse_x, mouse_y):
                    pause_continue.callback()
                    is_pause_menu = False
                    is_running = True
                    echo('Exiting Pause mode from Continue Button')

                # Stats
                if pause_stats.collidepoint(mouse_x, mouse_y):
                    is_game_stat = True

                # Exiting
                if pause_exit.collidepoint(mouse_x, mouse_y):
                    pause_exit.callback
                    is_end_screen = True
                    is_pause_menu = False
                    echo('Ending Game from Exit Button')

            elif is_end_screen:
                if end_cancel.collidepoint(mouse_x, mouse_y):
                    end_cancel.callback()

                    is_main_menu = True
                    is_end_screen = False

                    end_submit.is_active = True
                    end_submit.label = 'Submit'
                    end_name_field.is_active = True

                    highscore_name = 'Slimy'
                    score = 0
                    score_mult = 10
                    total_clicks = 0
                    passed_time = 0
                    hits = 0

                if end_submit.collidepoint(mouse_x, mouse_y):
                    end_submit.callback()
                    end_submit.label = 'Submitted'
                    end_submit.is_active = False
                    end_name_field.is_active = False

                    highscores[game_mode].append(Highscore(game_mode, highscore_name, score, time.strftime('%Y/%m/%d')))
                    highscores[game_mode].sort(key=lambda hs: float(hs.score), reverse=True)
                    echo(highscores, True)

                if end_name_field.collidepoint(mouse_x, mouse_y):
                    end_name_field.callback()
                    is_writing_highscore_name = True
                else:
                    is_writing_highscore_name = False

        elif event.type == MOUSEBUTTONUP:
            mouse_x, mouse_y = event.pos
            is_hitting = False

        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                if is_main_menu:
                    if is_settings_menu:
                        is_settings_menu = False
                        echo('Exiting Settings from ESC')
                    if is_credits:
                        is_credits = False
                        echo('Exiting Credits from ESC')
                    if is_highscore:
                        is_highscore = False
                        echo('Exiting Highscore from ESC')
                if is_running:
                    is_running = False
                    is_pause_menu = True
                    echo("Pausing Play Mode")
                elif is_pause_menu:
                    is_pause_menu = False
                    is_running = True
                    echo("Continuing Play Mode")

            if is_writing_highscore_name:
                if event.key == K_RETURN:
                    is_writing_highscore_name = False
                elif event.key == K_BACKSPACE:
                    highscore_name = highscore_name[0:-1]
                key = event.unicode
                if key.isalnum() or key.isspace():
                    highscore_name += key

        elif event.type == CREATURE_HIT_EVENT:
            hits += 1
            score_mult += 2
            score += get_cur_multiplier() + (3 - event.message.type)
            creature_arr.sprites.remove(event.message.anim)
            creature_arr.sprites.add(event.message.ko_anim)

    # Drawing
    bg.update()
    bg.draw(window)
    if is_main_menu:
        if is_settings_menu:

            # draw Menu Title
            pygame.draw.rect(window, Color.GREEN, (win_x * 0.3, win_y * 0.05, win_x * 0.4, win_y * 0.125))
            pygame.draw.rect(window, Color.BLACK, (win_x * 0.3, win_y * 0.05, win_x * 0.4, win_y * 0.125), 5)

            title_surf, title_rect = text_objects('Tuning', largeText)
            title_rect.center = (win_x / 2, win_y * 0.115)
            window.blit(title_surf, title_rect)

            # draw Menu Items
            pygame.draw.rect(window, Color.GREEN, (win_x * 0.3, win_y * 0.225, win_x * 0.4, win_y * 0.65))
            pygame.draw.rect(window, Color.BLACK, (win_x * 0.3, win_y * 0.225, win_x * 0.4, win_y * 0.65), 5)

            mode_surf, mode_rect = text_objects('Game Mode', mediumText)
            size_surf, size_rect = text_objects('Slime Size', mediumText)
            speed_surf, speed_rect = text_objects('Dissipation Speed', mediumText)
            prob_surf, prob_rect = text_objects('Pollution Level', mediumText)

            mode_rect.center = (win_x / 2, win_y * 0.275)
            size_rect.center = (win_x / 2, win_y * 0.425)
            speed_rect.center = (win_x / 2, win_y * 0.575)
            prob_rect.center = (win_x / 2, win_y * 0.725)

            window.blit(mode_surf, mode_rect)
            window.blit(size_surf, size_rect)
            window.blit(speed_surf, speed_rect)
            window.blit(prob_surf, prob_rect)

            settings_mode.draw(window)
            settings_grid.draw(window)
            settings_speed.draw(window)
            settings_frequenzy.draw(window)

            # draw back button
            color = Color.GREEN_BRIGHT if win_x * 0.01 <= mouse_x <= win_x * 0.06 and \
                                          win_y * 0.01 <= mouse_y <= win_y * 0.01 + win_x * 0.05 \
                else Color.GREEN
            back_button(window, color, (int(win_x * 0.035), int(win_x * 0.035)), int(win_x * 0.025))

        elif is_highscore:
            # back button
            color = Color.GREEN_BRIGHT if win_x * 0.01 <= mouse_x <= win_x * 0.06 \
                                          and win_y * 0.01 <= mouse_y <= win_y * 0.01 + win_x * 0.05 \
                else Color.GREEN
            back_button(window, color, (int(win_x * 0.035), int(win_x * 0.035)), int(win_x * 0.025))

            # draw menu title
            pygame.draw.rect(window, Color.GREEN, (win_x * 0.35, win_y * 0.15, win_x * 0.3, win_y * 0.1))
            pygame.draw.rect(window, Color.BLACK, (win_x * 0.35, win_y * 0.15, win_x * 0.3, win_y * 0.1), 5)

            title_surf, title_rect = text_objects("Hall o' Fame", mediumText)
            title_rect.center = (win_x * 0.5, win_y * 0.2)
            window.blit(title_surf, title_rect)

            # draw highscore text

            pygame.draw.rect(window, Color.GREEN, (win_x * 0.175, win_y * 0.3, win_x * 0.65, win_y * 0.575))
            pygame.draw.rect(window, Color.BLACK, (win_x * 0.175, win_y * 0.3, win_x * 0.65, win_y * 0.575), 5)

            # draw mode selection
            highscore_select.draw(window)

            for i in range(len(highscores[highscore_mode])):
                text_surf, text_rect = text_objects(highscores[highscore_mode][i].name.ljust(max_chars + 5) + str(
                    int(float(highscores[highscore_mode][i].score))).rjust(max_chars + 2) + highscores[highscore_mode]
                                                    [i].date.rjust(13), high_score_text)
                text_rect.topleft = (win_x * 0.2, win_y * 0.375 + win_y * 0.05 * i)
                window.blit(text_surf, text_rect)

        elif is_credits:
            # draw menu title
            pygame.draw.rect(window, Color.GREEN, (win_x * 0.35, win_y * 0.2, win_x * 0.3, win_y * 0.1))
            pygame.draw.rect(window, Color.BLACK, (win_x * 0.35, win_y * 0.2, win_x * 0.3, win_y * 0.1), 5)

            title_surf, title_rect = text_objects('Credits', mediumText)
            title_rect.center = (win_x * 0.5, win_y * 0.25)
            window.blit(title_surf, title_rect)

            # draw credits text

            round_rect(window, Color.GREEN, (win_x * 0.175, win_y * 0.35, win_x * 0.65, win_y * 0.5),
                       int(win_x * 0.01))
            round_rect(window, Color.BLACK, (win_x * 0.175, win_y * 0.35, win_x * 0.65, win_y * 0.5), int(win_x * 0.01),
                       5)

            texts = ('This certainly exciting, at times potentially',
                     'annoying Game, as you seek to gain Leadership',
                     'other all Slime Smashers',
                     'has been fumbly created by the humble byte magician')

            # draw back button
            color = Color.GREEN_BRIGHT if win_x * 0.01 <= mouse_x <= win_x * 0.06 \
                                          and win_y * 0.01 <= mouse_y <= win_y * 0.01 + win_x * 0.05 \
                else Color.GREEN
            back_button(window, color, (int(win_x * 0.035), int(win_x * 0.035)), int(win_x * 0.025))

            for i in range(len(texts)):
                text_surf, text_rect = text_objects(texts[i], settings_text)
                text_rect.topleft = (win_x * 0.2, win_y * 0.4 + win_y * 0.0375 * i)
                window.blit(text_surf, text_rect)

            name_surf, name_rect = text_objects('Exxcelius', mediumText)
            name_rect.midtop = (win_x * 0.5, win_y * 0.4 + win_y * 0.0375 * len(texts))
            window.blit(name_surf, name_rect)

        else:
            # draw Button Objects
            main_play.draw(window, (mouse_x, mouse_y))
            main_highscore.draw(window, (mouse_x, mouse_y))
            main_credits.draw(window, (mouse_x, mouse_y))

            # draw name
            pygame.draw.rect(window, Color.GREEN, main_title)
            pygame.draw.rect(window, Color.BLACK, main_title, 5)

            TextSurf, TextRect = text_objects("Slash the Slime!", largeText)
            TextRect.center = ((win_x / 2), (win_y * 0.295))
            window.blit(TextSurf, TextRect)

            # draw settings button
            if main_settings.collidepoint(mouse_x, mouse_y):
                pygame.draw.circle(window, Color.GREEN_BRIGHT, (int(win_x * 0.09), int(win_x * 0.035)),
                                   int(win_x * 0.0225))
            else:
                pygame.draw.circle(window, Color.GREEN, (int(win_x * 0.09), int(win_x * 0.035)), int(win_x * 0.015))

            settings_icon = pygame.image.load(os.path.join(image_folder, 'Settings_button.png')).convert()
            settings_icon.set_colorkey((255,255,255))
            settings_icon = pygame.transform.scale(settings_icon, (int(win_x * 0.05), int(win_x * 0.05)))
            window.blit(settings_icon, (win_x * 0.065, win_x * 0.01))

            # draw exit button
            if win_x * 0.01 <= mouse_x <= win_x * 0.06 and win_y * 0.01 <= mouse_y <= win_y * 0.01 + win_x * 0.05:
                power_icon = pygame.image.load(os.path.join(image_folder, 'on-off-power-button-icon_on.png'))
            else:
                power_icon = pygame.image.load(os.path.join(image_folder, 'on-off-power-button-icon_off.png'))

            power_icon = pygame.transform.scale(power_icon, (int(win_x * 0.05), int(win_x * 0.05)))
            window.blit(power_icon, (win_x * 0.01, win_x * 0.01))

    else:  # not is_main_menu
        text = ''
        if game_mode == 0 or game_mode == 2:  # Classic and Sniper
            text = '%.1f%%' % ((hits / total_clicks * 100) if total_clicks != 0 else 1.0)
        elif game_mode == 1:  # Limited Stamina
            text = str(max_clicks - total_clicks)
        elif game_mode == 3:  # Deathmatch:
            time_left = max_time - passed_time
            text = f'{time_left // 60} : {time_left % 60}'

        end_surf, end_rect = text_objects(text, mediumText, (200, 200, 200))
        score_surf, score_rect = text_objects(f'Score: {int(score)}', mediumText, (200, 200, 200))
        mult_surf, mult_rect = text_objects('x %.2f' % score_mult_text, mediumText, (200, 200, 200))

        end_rect.midleft = (win_y * 0.075 / 2 - 2, win_y * 0.075 / 2 + 2)
        score_rect.center = (win_x / 2, win_y * 0.075 / 2 + 2)
        mult_rect.midright = (win_x - win_y * 0.075 / 2 - 2, win_y * 0.075 / 2 + 2)

        window.blit(end_surf, end_rect)
        window.blit(score_surf, score_rect)
        window.blit(mult_surf, mult_rect)

        # draw creatures
        creature_arr.draw(window)

        if is_pause_menu:

            if is_stat_menu:
                pass

            else:
                # draw Menu Title
                pygame.draw.rect(window, Color.GREEN, (win_x * 0.3, win_y * 0.1, win_x * 0.4, win_y * 0.125))
                pygame.draw.rect(window, Color.BLACK, (win_x * 0.3, win_y * 0.1, win_x * 0.4, win_y * 0.125), 5)

                title_surf, title_rect = text_objects('Resting', largeText)
                title_rect.center = (win_x / 2, win_y * 0.165)
                window.blit(title_surf, title_rect)

                pygame.draw.rect(window, Color.GREEN, (win_x * 0.3, win_y * 0.275, win_x * 0.4, win_y * 0.575))
                pygame.draw.rect(window, Color.BLACK, (win_x * 0.3, win_y * 0.275, win_x * 0.4, win_y * 0.575), 5)

                # draw Menu Items
                pause_continue.draw(window, (mouse_x, mouse_y))
                pause_stats.draw(window, (mouse_x, mouse_y))
                pause_exit.draw(window, (mouse_x, mouse_y))

                # draw back button
                if win_x * 0.01 <= mouse_x <= win_x * 0.06 and win_y * 0.01 <= mouse_y <= win_y * 0.01 + win_x * 0.05:
                    back_button(window, Color.GREEN_BRIGHT, (int(win_x * 0.035), int(win_x * 0.035)),
                                int(win_x * 0.025))
                else:
                    back_button(window, Color.GREEN, (int(win_x * 0.035), int(win_x * 0.035)), int(win_x * 0.025))

        if is_end_screen:
            # draw Menu Title
            pygame.draw.rect(window, Color.GREEN, (win_x * 0.3, win_y * 0.1, win_x * 0.4, win_y * 0.125))
            pygame.draw.rect(window, Color.BLACK, (win_x * 0.3, win_y * 0.1, win_x * 0.4, win_y * 0.125), 5)

            title_surf, title_rect = text_objects('You lost', largeText)
            title_rect.center = (win_x / 2, win_y * 0.165)
            window.blit(title_surf, title_rect)

            # draw panel
            pygame.draw.rect(window, Color.GREEN, (win_x * 0.3, win_y * 0.275, win_x * 0.4, win_y * 0.575))
            pygame.draw.rect(window, Color.BLACK, (win_x * 0.3, win_y * 0.275, win_x * 0.4, win_y * 0.575), 5)

            # draw Buttons
            end_name_field.draw(window, (mouse_x, mouse_y))
            if is_writing_highscore_name and end_submit.is_active:
                pygame.draw.rect(window, Color.WHITE, end_text_line)
                pygame.draw.rect(window, Color.BLACK, end_text_line_border, 5)
            name_surf, name_rect = text_objects(highscore_name,
                                                pygame.font.Font('freesansbold.ttf', int(win_y * 0.1 - win_x * 0.02)))
            name_rect.topleft = (win_x * 0.36 + 3, win_y * 0.435 + win_x * 0.01 + 3)
            window.blit(name_surf, name_rect)
            end_submit.draw(window, (mouse_x, mouse_y))
            end_cancel.draw(window, (mouse_x, mouse_y))

        if is_running:
            pass

    # draw cursor
    if is_hitting:
        window.blit(hammer_down, (mouse_x - win_x * 0.0225, mouse_y - win_y * 0.025))
    else:
        window.blit(hammer_up, (mouse_x - win_x * 0.02, mouse_y - win_x * 0.03))

    pygame.display.flip()
    fpsClock.tick(fps)
