import os
import threading as thr
import time

import requests
import sqlite3

from tkinter import *
from tkinter import messagebox, ttk
from tkinter.font import nametofont


is_debug = False
running_records = []
max_parallel = 3
supported_file_types = ['mp3', 'm4a', 'ogg', 'wav', 'wma', 'aac', 'tta']
main = os.path.dirname(__file__)
path = os.path.join(main, 'data.db')

def echo(*args, **kwargs):
    if is_debug:
        print(args, kwargs)

class Recorder(thr.Thread):

    def __init__(self, url: str, file_name: str, duration: int, on_finish, break_event: thr.Event):
        """

        :param url:
        :param file_name:
        :param duration:
        :param on_finish:
        :param break_event: Event to be set if the Recording is to be finished prematurly
        """
        super().__init__()
        self.url = url
        self.file_name = file_name
        self.duration = duration
        self.on_finished = on_finish
        self.break_event = break_event

        self.start_time = 0
        self.passed_time = 0

    def run(self):
        self.record_audio()

        self.duration = self.passed_time
        with open(path, 'a') as file:
            file.write(f'{time.strftime("%Y-%m-%d", self.start_time)},{time.strftime("%H:%M:%S", self.start_time)},' +
                       f'{self.duration},{os.path.join(os.getcwd(), self.file_name)},{self.url}')

        self.on_finished(self)

    def record_audio(self):
        self.start_time = time.localtime()

        rec_file = open(self.file_name, 'wb')
        chunk_size = 1024

        start_time_in_seconds = time.time()

        time_limit = self.duration
        self.passed_time = 0
        with requests.Session() as session:
            try:
                response = session.get(self.url, stream=True)
                for chunk in response.iter_content(chunk_size=chunk_size):
                    if self.duration > 0 and self.passed_time > time_limit or self.break_event is not None and self.break_event.is_set():
                        break
                    # to print time elapsed
                    if int(time.time() - start_time_in_seconds) - self.passed_time > 0:
                        self.passed_time = int(time.time() - start_time_in_seconds)
                    if chunk:
                        rec_file.write(chunk)
            finally:
                if not rec_file.closed:
                    rec_file.close()


class AddRecordFrame(Toplevel):
    """"""

    # ----------------------------------------------------------------------
    def __init__(self, original, on_add, on_finish):
        """Constructor"""
        self.original_frame = original
        Toplevel.__init__(self)

        self.on_add = on_add
        self.on_finish = on_finish

        self.title("Add Recorder")

        # Arguments
        win = Frame(self)
        win.pack(fill=BOTH, expand=1)

        url_lb = Label(win, text='URL:' + ' ' * 6)
        self.url = Text(win, height=1, width=50)

        file_name_lb = Label(win, text='File Name:')
        self.file_name = Text(win, height=1, width=50)

        duration_lb = Label(win, text='Duration:')
        self.duration = Text(win, height=1, width=50)

        url_lb.grid(row=0, column=0, sticky=W)
        file_name_lb.grid(row=1, column=0, sticky=W)
        duration_lb.grid(row=2, column=0, sticky=W)

        self.url.grid(row=0, column=1)
        self.file_name.grid(row=1, column=1)
        self.duration.grid(row=2, column=1)

        btns = Frame(win)
        close = Button(btns, text="Close", command=self.onClose)
        add = Button(btns, text='Add', command=self.onAdd)
        add.pack(side='right')
        close.pack(side='right')
        btns.grid(row=3, column=1, sticky=E)

    # ----------------------------------------------------------------------
    def close(self):
        self.destroy()
        self.original_frame.show()

    def onClose(self):
        self.close()

    def onAdd(self):
        import re
        import requests
        url = self.url.get('1.0', END)
        f_name = self.file_name.get('1.0', END)[0:-1]
        dur = self.duration.get('1.0', END)
        if dur == '\n':
            dur = 0
        else:
            try:
                dur = int(dur)
            except:
                messagebox.showerror('Duration Error', 'Please enter a valid Duration in seconds')

        if 'http' not in url[:4]:
            url = 'http://' + url
        try:
            r = requests.Session().get(url, stream=True)
            r.close()
        except requests.exceptions.MissingSchema:
            messagebox.showerror('Connection Error', 'The specified Url does not seem to fit the necessary Schema.')
            return
        except requests.exceptions.ConnectionError:
            messagebox.showerror('Connection Error', 'Connecting to the specified Url seems impossible')
            return

        if 'audio' not in r.headers['Content-Type']:
            messagebox.showerror('Wrong Content', 'The specified Url does not point to an audio stream.')

        name_parts = re.split('\.', f_name)
        echo(name_parts)
        if name_parts[-1] not in supported_file_types:
            messagebox.showerror('Error', "The Filetype you've chosen is not supported. Supported are " + str(
                supported_file_types))
            echo('Filetype not supported')
            return

        f_name = os.path.join(main, f_name)

        self.on_add(Recorder(url, f_name, dur, self.on_finish, thr.Event()))
        self.close()


class InfoFrame(Toplevel):

    def __init__(self, original, title='Info', message='', exit_label='OK'):
        """

        :param title:
        :param message:
        :param exit_label:
        """
        self.original_frame = original
        Toplevel.__init__(self)
        self.title(title)

        win = Frame(self)
        win.pack(fill=BOTH, expand=1)
        texts = re.split('\n', message)
        for i in range(len(texts)):
            panel = Label(win, text=texts[i], font='DejaVu')
            panel.grid(row=i, sticky=W)

        exit_btn = Button(win, text=exit_label, command=self.close)
        exit_btn.grid(row=len(texts), sticky=E)

    def close(self):
        self.destroy()
        self.original_frame.show()


class MyApp(object):
    """"""

    # ----------------------------------------------------------------------
    def __init__(self, parent: Tk, db_cursor: sqlite3.Connection):
        """Constructor"""
        default_font = nametofont('TkDefaultFont')
        default_font.configure(family='DejaVu')

        self.root = parent
        self.root.title("Cli.py")
        self.frame = Frame(parent)
        self.frame.pack(fill=X)

        self.db = db_cursor

        self.running_records = []
        self.pending_records = []
        self.finished_records = []

        self.db.execute("SELECT * FROM Records")
        self.past_records = []
        for row in self.db:
            self.past_records.append(row)

        self.list_frame = Frame(self.frame)
        self.list_frame.pack()

        menu = Menu(self.frame)
        self.root.config(menu=menu)
        record_menu = Menu(menu)
        menu.add_cascade(label="Record", menu=record_menu)
        record_menu.add_command(label="New", command=self.open_frame)
        record_menu.add_separator()
        record_menu.add_command(label="Exit", command=self.root.quit)

        settings_menu = Menu(menu)
        menu.add_cascade(label='Settings', menu=settings_menu)

        helpmenu = Menu(menu)
        menu.add_cascade(label="Help", menu=helpmenu)
        helpmenu.add_command(label="About...", command=self.about)

        self.display = Frame(self.frame)
        self.display.pack()

        self.draw_record_list()

    # ----------------------------------------------------------------------
    def hide(self):
        """"""
        self.root.withdraw()

    # ----------------------------------------------------------------------
    def open_frame(self):
        """"""
        subframe = AddRecordFrame(self, self.add_record, self.finished_record)

    # ----------------------------------------------------------------------
    def show(self):
        """"""
        self.root.update()
        self.root.deiconify()

    # ----------------------------------------------------------------------
    def about(self):
        """"""
        text = 'This program lets you record any mp3 stream.\n'
        text += 'To start a new recording go to Record -> add. \n'
        text += "A new Window will open, where you can enter the stream's \nURL, "
        text += 'the filename under which you wish to save the recording, \n'
        text += 'and the desired Duration of the Recording.\n'
        text += 'Entering 0 will record until you actively stop that recording.\n \n'
        text += "The supported file types for saving recordings are 'mp3', \n'm4a', 'ogg', 'wav', 'wma', 'aac', 'tta'"
        about = InfoFrame(self, 'About', text, 'Got it!')

    # ----------------------------------------------------------------------
    def update_records(self):
        while len(self.running_records) < max_parallel and len(self.pending_records) > 0:
            rec = self.pending_records[0]
            self.running_records.append(rec)
            self.pending_records.remove(rec)
            rec.start()
        self.draw_record_list()

    # ----------------------------------------------------------------------
    def add_record(self, record):
        self.pending_records.append(record)
        self.update_records()

    # ----------------------------------------------------------------------
    def finished_record(self, record: Recorder):
        if record in self.running_records:
            self.running_records.remove(record)

            date = time.strftime("%Y-%m-%d")
            clock = time.strftime("%H:%M:%S", time.localtime())
            duration = record.duration
            path = os.path.join(os.getcwd(), record.file_name)
            url = record.url
            self.finished_records.append((date, clock, duration, path, url))
        elif record in self.pending_records:
            self.pending_records.remove(record)

        self.draw_record_list()

    def draw_record_list(self):

        def on_click(event):
            children = tree.get_children()
            count = len(children)
            row = children.index(tree.identify_row(event.y))
            col = tree.identify_column(event.x)
            echo(children, count, row)
            if row >= len(self.running_records) or col != '#1':
                echo('No Stop Button\n')
            else:
                echo('Stop Button\n')
                self.running_records[row].break_event.set()

        self.list_frame.destroy()
        self.list_frame = Frame(self.frame)
        self.list_frame.pack(side='top', fill=BOTH, expand=1)

        scrollbarx = Scrollbar(self.list_frame, orient=HORIZONTAL)
        scrollbary = Scrollbar(self.list_frame, orient=VERTICAL)
        tree = ttk.Treeview(self.list_frame, columns=('Button', 'Duration', 'URL', 'Filename', 'Date', 'Time'),
                            height=400,
                            selectmode="extended",
                            yscrollcommand=scrollbary.set, xscrollcommand=scrollbarx.set)
        scrollbary.config(command=tree.yview)
        scrollbary.pack(side=RIGHT, fill=Y)
        scrollbarx.config(command=tree.xview)
        scrollbarx.pack(side=BOTTOM, fill=X)

        tree.heading('Button', text=" ", anchor=W)
        tree.heading('Duration', text="Duration", anchor=W)
        tree.heading('URL', text='URL', anchor=W)
        tree.heading('Filename', text='Filename', anchor=W)
        tree.heading('Date', text="Date", anchor=W)
        tree.heading('Time', text="Time", anchor=W)
        tree.column('#0', stretch=NO, minwidth=0, width=0)
        tree.column('#1', stretch=NO, minwidth=0, width=20)
        tree.column('#2', stretch=NO, minwidth=0, width=70)
        tree.column('#3', stretch=NO, minwidth=0, width=300)
        tree.column('#4', stretch=NO, minwidth=0, width=400)
        tree.column('#5', stretch=NO, minwidth=0, width=100)
        tree.column('#6', stretch=NO, minwidth=0, width=75)

        tree.pack()
        tree.bind('<Button-1>', on_click)

        # FIlling the Tree

        for row in self.past_records:
            tree.insert('', 0, values=('', int(row[2]), row[4], row[3], row[0], row[1]))

        for row in self.finished_records:
            tree.insert('', 0, values=('', row[2], row[4], row[3], row[0], row[1]))

        for rec in self.pending_records:
            echo(type(rec))
            tree.insert('', 0, values=('\u29D6', rec.duration, rec.url, rec.file_name))

        for rec in self.running_records[::-1]:
            echo(type(rec))
            tree.insert('', 0, values=('\u25FE', '\u221E' if rec.duration <= 0 else rec.duration,
                                       rec.url, rec.file_name))


# ----------------------------------------------------------------------


if __name__ == "__main__":
    
    conn = sqlite3.connect(path)
    c = conn.cursor()

    root = Tk()
    root.geometry("800x600")
    root.resizable(0, 0)

    app = MyApp(root, c)

    root.mainloop()

    for rec in app.pending_records:
        rec.break_event.set()

    for rec in app.running_records:
        if rec.duration <= 0:
            rec.break_event.set()

    c.executemany("INSERT INTO Records VALUES (?,?,?,?,?)", app.finished_records)
    conn.commit()
    conn.close()

